<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Services\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @var SearchService
     */
    protected $searchService;

    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }


    /**
    * @OA\Post(
    *      path="/api/search",
    *      operationId="search",
    *      tags={"Search"},
    *      summary="Super App Searcher",
    *      description="Returns Search Result",
    * @OA\RequestBody( 
    *      required=true,    
 *         @OA\MediaType(
 *             mediaType="application/x-www-form-urlencoded",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="from_n",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="ret_count",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="pletterid",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="prandomid",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="fdate",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="tdate",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="psearch",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="ptag",
 *                     type="string"
 *                 ),
 *                 
 *             ),
 *         ),
 *     ),
    * @OA\Response(
    *    response=200, 
    *    description="successful operation",
    *    @OA\JsonContent(
    *              @OA\Property(
 *                     property="LETTERS_ID",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="RANDOMID",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="LETTERID",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="LETTERTEMPLATEID",
 *                     type="integer"
 *                 ),
 *                 @OA\Property(
 *                     property="SENDER",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="REGISTRATIONDATE",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="REGISTRATIONNUMBER",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="ORIGINALDATE",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="ORIGINALNUMBER",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="NBSPATH",
 *                     type="string"
 *                 ),
    *     ),
    *    ),
    * @OA\Response(response=400, description="Bad request"),
    * )
    *
    * Returns search result
    */

    public function search(SearchRequest $request)
    {
        $result = $this->searchService->superAppSearcher($request->all());
        return response()->json($result);
    }


    /**
    * @OA\Post(
    *      path="/api/getTags",
    *      operationId="getTags",
    *      tags={"getTags"},
    *      summary="Get Tags",
    *      description="Returns Tags",
    * @OA\RequestBody( 
    *      required=true,    
    *         @OA\MediaType(
    *             mediaType="application/x-www-form-urlencoded",
    *             @OA\Schema(
    *                 @OA\Property(
    *                     property="pletters_id",
    *                     type="integer"
    *                 ),
    *             ),
    *         ),
    *     ),
    * @OA\Response(
    *    response=200, 
    *    description="successful operation",
    *    @OA\JsonContent(
    *      @OA\Property(
    *       property="ID",
    *       type="integer"
    *      ),
    *      @OA\Property(
    *       property="TEXT",
    *       type="string"
    *      ),
    *     ),
    *    ),
    * @OA\Response(response=400, description="Bad request"),
    * )
    *
    * Returns search result
    */
    public function getTags(Request $request)
    {
        $result = $this->searchService->getTags($request->all());
        return response()->json($result);
    }
}
