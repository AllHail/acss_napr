<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class SearchService 
{
    /**
     * @var PDOConnection
     */
    protected $pdo;

    /**
     * @var resource
     */
    protected $resource;

    /**
     * @construct
     */
    public function __construct()
    {
        $this->pdo = DB::getPdo();
        $this->resource = $this->pdo->getResource();
    }

    public function getTags(array $input)
    {
        $query = 'begin
                    -- Call the function
                    :result := acss.search.get_tags(pletters_id => :pletters_id);
                end;';


        $stid = oci_parse($this->resource, $query);
        $p_cursor = oci_new_cursor($this->resource);

        // Input
        oci_bind_by_name($stid, ':pletters_id', $input['pletters_id']);

        // Output
        oci_bind_by_name($stid, ':result', $p_cursor, -1, OCI_B_CURSOR);

        oci_execute($stid);
        oci_execute($p_cursor, OCI_DEFAULT);

        oci_fetch_all($p_cursor, $cursor, null, null, OCI_FETCHSTATEMENT_BY_ROW);

        return $cursor;
    }

    public function superAppSearcher(array $input)
    {
        $query = 'begin
                    -- Call the function
                    :result := acss.search.super_app_searcher(
                        from_n => :from_n,
                        ret_count => :ret_count,
                        pletterid => :pletterid,
                        prandomid => :prandomid,
                        fdate => :fdate,
                        tdate => :tdate,
                        psearch => :psearch,
                        ptag => :ptag,
                        prec_count => :prec_count);
                end;';


        $stid = oci_parse($this->resource, $query);
        $p_cursor = oci_new_cursor($this->resource);
        $prec_count = 0;

        // Input
        oci_bind_by_name($stid, ':from_n', $input['from_n']);
        oci_bind_by_name($stid, ':ret_count', $input['ret_count']);
        oci_bind_by_name($stid, ':pletterid', $input['pletterid']);
        oci_bind_by_name($stid, ':prandomid', $input['prandomid']);
        oci_bind_by_name($stid, ':fdate', $input['fdate']);
        oci_bind_by_name($stid, ':tdate', $input['tdate']);
        oci_bind_by_name($stid, ':psearch', $input['psearch']);
        oci_bind_by_name($stid, ':ptag', $input['ptag']);

        // Output
        oci_bind_by_name($stid, ':prec_count', $prec_count, 32);
        oci_bind_by_name($stid, ':result', $p_cursor, -1, OCI_B_CURSOR);

        oci_execute($stid);
        oci_execute($p_cursor, OCI_DEFAULT);

        oci_fetch_all($p_cursor, $cursor, null, null, OCI_FETCHSTATEMENT_BY_ROW);

        return $cursor;
    }
}
