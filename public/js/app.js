/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/axios/index.js":
/*!*************************************!*\
  !*** ./node_modules/axios/index.js ***!
  \*************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! ./lib/axios */ "./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/adapters/xhr.js ***!
  \************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var settle = __webpack_require__(/*! ./../core/settle */ "./node_modules/axios/lib/core/settle.js");
var cookies = __webpack_require__(/*! ./../helpers/cookies */ "./node_modules/axios/lib/helpers/cookies.js");
var buildURL = __webpack_require__(/*! ./../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");
var buildFullPath = __webpack_require__(/*! ../core/buildFullPath */ "./node_modules/axios/lib/core/buildFullPath.js");
var parseHeaders = __webpack_require__(/*! ./../helpers/parseHeaders */ "./node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__(/*! ./../helpers/isURLSameOrigin */ "./node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__(/*! ../core/createError */ "./node_modules/axios/lib/core/createError.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;
    var responseType = config.responseType;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password ? unescape(encodeURIComponent(config.auth.password)) : '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    var fullPath = buildFullPath(config.baseURL, config.url);
    request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    function onloadend() {
      if (!request) {
        return;
      }
      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !responseType || responseType === 'text' ||  responseType === 'json' ?
        request.responseText : request.response;
      var response = {
        data: responseData,
        status: request.status,
        statusText: request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    }

    if ('onloadend' in request) {
      // Use onloadend if available
      request.onloadend = onloadend;
    } else {
      // Listen for ready state to emulate onloadend
      request.onreadystatechange = function handleLoad() {
        if (!request || request.readyState !== 4) {
          return;
        }

        // The request errored out and we didn't get a response, this will be
        // handled by onerror instead
        // With one exception: request that using file: protocol, most browsers
        // will return status as 0 even though it's a successful request
        if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
          return;
        }
        // readystate handler is calling before onerror or ontimeout handlers,
        // so we should call onloadend on the next 'tick'
        setTimeout(onloadend);
      };
    }

    // Handle browser request cancellation (as opposed to a manual cancellation)
    request.onabort = function handleAbort() {
      if (!request) {
        return;
      }

      reject(createError('Request aborted', config, 'ECONNABORTED', request));

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      var timeoutErrorMessage = 'timeout of ' + config.timeout + 'ms exceeded';
      if (config.timeoutErrorMessage) {
        timeoutErrorMessage = config.timeoutErrorMessage;
      }
      reject(createError(
        timeoutErrorMessage,
        config,
        config.transitional && config.transitional.clarifyTimeoutError ? 'ETIMEDOUT' : 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ?
        cookies.read(config.xsrfCookieName) :
        undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (!utils.isUndefined(config.withCredentials)) {
      request.withCredentials = !!config.withCredentials;
    }

    // Add responseType to request if needed
    if (responseType && responseType !== 'json') {
      request.responseType = config.responseType;
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (!requestData) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/axios.js ***!
  \*****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__(/*! ./core/Axios */ "./node_modules/axios/lib/core/Axios.js");
var mergeConfig = __webpack_require__(/*! ./core/mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");
var defaults = __webpack_require__(/*! ./defaults */ "./node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(mergeConfig(axios.defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__(/*! ./cancel/Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__(/*! ./cancel/CancelToken */ "./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__(/*! ./cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__(/*! ./helpers/spread */ "./node_modules/axios/lib/helpers/spread.js");

// Expose isAxiosError
axios.isAxiosError = __webpack_require__(/*! ./helpers/isAxiosError */ "./node_modules/axios/lib/helpers/isAxiosError.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports["default"] = axios;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/*!*************************************************!*\
  !*** ./node_modules/axios/lib/cancel/Cancel.js ***!
  \*************************************************/
/***/ ((module) => {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/cancel/CancelToken.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var Cancel = __webpack_require__(/*! ./Cancel */ "./node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/cancel/isCancel.js ***!
  \***************************************************/
/***/ ((module) => {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/*!**********************************************!*\
  !*** ./node_modules/axios/lib/core/Axios.js ***!
  \**********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var buildURL = __webpack_require__(/*! ../helpers/buildURL */ "./node_modules/axios/lib/helpers/buildURL.js");
var InterceptorManager = __webpack_require__(/*! ./InterceptorManager */ "./node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__(/*! ./dispatchRequest */ "./node_modules/axios/lib/core/dispatchRequest.js");
var mergeConfig = __webpack_require__(/*! ./mergeConfig */ "./node_modules/axios/lib/core/mergeConfig.js");
var validator = __webpack_require__(/*! ../helpers/validator */ "./node_modules/axios/lib/helpers/validator.js");

var validators = validator.validators;
/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = arguments[1] || {};
    config.url = arguments[0];
  } else {
    config = config || {};
  }

  config = mergeConfig(this.defaults, config);

  // Set config.method
  if (config.method) {
    config.method = config.method.toLowerCase();
  } else if (this.defaults.method) {
    config.method = this.defaults.method.toLowerCase();
  } else {
    config.method = 'get';
  }

  var transitional = config.transitional;

  if (transitional !== undefined) {
    validator.assertOptions(transitional, {
      silentJSONParsing: validators.transitional(validators.boolean, '1.0.0'),
      forcedJSONParsing: validators.transitional(validators.boolean, '1.0.0'),
      clarifyTimeoutError: validators.transitional(validators.boolean, '1.0.0')
    }, false);
  }

  // filter out skipped interceptors
  var requestInterceptorChain = [];
  var synchronousRequestInterceptors = true;
  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    if (typeof interceptor.runWhen === 'function' && interceptor.runWhen(config) === false) {
      return;
    }

    synchronousRequestInterceptors = synchronousRequestInterceptors && interceptor.synchronous;

    requestInterceptorChain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  var responseInterceptorChain = [];
  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    responseInterceptorChain.push(interceptor.fulfilled, interceptor.rejected);
  });

  var promise;

  if (!synchronousRequestInterceptors) {
    var chain = [dispatchRequest, undefined];

    Array.prototype.unshift.apply(chain, requestInterceptorChain);
    chain = chain.concat(responseInterceptorChain);

    promise = Promise.resolve(config);
    while (chain.length) {
      promise = promise.then(chain.shift(), chain.shift());
    }

    return promise;
  }


  var newConfig = config;
  while (requestInterceptorChain.length) {
    var onFulfilled = requestInterceptorChain.shift();
    var onRejected = requestInterceptorChain.shift();
    try {
      newConfig = onFulfilled(newConfig);
    } catch (error) {
      onRejected(error);
      break;
    }
  }

  try {
    promise = dispatchRequest(newConfig);
  } catch (error) {
    return Promise.reject(error);
  }

  while (responseInterceptorChain.length) {
    promise = promise.then(responseInterceptorChain.shift(), responseInterceptorChain.shift());
  }

  return promise;
};

Axios.prototype.getUri = function getUri(config) {
  config = mergeConfig(this.defaults, config);
  return buildURL(config.url, config.params, config.paramsSerializer).replace(/^\?/, '');
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(mergeConfig(config || {}, {
      method: method,
      url: url,
      data: (config || {}).data
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(mergeConfig(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/core/InterceptorManager.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected, options) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected,
    synchronous: options ? options.synchronous : false,
    runWhen: options ? options.runWhen : null
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ "./node_modules/axios/lib/core/buildFullPath.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/buildFullPath.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var isAbsoluteURL = __webpack_require__(/*! ../helpers/isAbsoluteURL */ "./node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__(/*! ../helpers/combineURLs */ "./node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Creates a new URL by combining the baseURL with the requestedURL,
 * only when the requestedURL is not already an absolute URL.
 * If the requestURL is absolute, this function returns the requestedURL untouched.
 *
 * @param {string} baseURL The base URL
 * @param {string} requestedURL Absolute or relative URL to combine
 * @returns {string} The combined full path
 */
module.exports = function buildFullPath(baseURL, requestedURL) {
  if (baseURL && !isAbsoluteURL(requestedURL)) {
    return combineURLs(baseURL, requestedURL);
  }
  return requestedURL;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/createError.js ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var enhanceError = __webpack_require__(/*! ./enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/core/dispatchRequest.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var transformData = __webpack_require__(/*! ./transformData */ "./node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__(/*! ../cancel/isCancel */ "./node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__(/*! ../defaults */ "./node_modules/axios/lib/defaults.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData.call(
    config,
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData.call(
      config,
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData.call(
          config,
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/core/enhanceError.js ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function toJSON() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code
    };
  };
  return error;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/mergeConfig.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/core/mergeConfig.js ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

/**
 * Config-specific merge-function which creates a new config-object
 * by merging two configuration objects together.
 *
 * @param {Object} config1
 * @param {Object} config2
 * @returns {Object} New object resulting from merging config2 to config1
 */
module.exports = function mergeConfig(config1, config2) {
  // eslint-disable-next-line no-param-reassign
  config2 = config2 || {};
  var config = {};

  var valueFromConfig2Keys = ['url', 'method', 'data'];
  var mergeDeepPropertiesKeys = ['headers', 'auth', 'proxy', 'params'];
  var defaultToConfig2Keys = [
    'baseURL', 'transformRequest', 'transformResponse', 'paramsSerializer',
    'timeout', 'timeoutMessage', 'withCredentials', 'adapter', 'responseType', 'xsrfCookieName',
    'xsrfHeaderName', 'onUploadProgress', 'onDownloadProgress', 'decompress',
    'maxContentLength', 'maxBodyLength', 'maxRedirects', 'transport', 'httpAgent',
    'httpsAgent', 'cancelToken', 'socketPath', 'responseEncoding'
  ];
  var directMergeKeys = ['validateStatus'];

  function getMergedValue(target, source) {
    if (utils.isPlainObject(target) && utils.isPlainObject(source)) {
      return utils.merge(target, source);
    } else if (utils.isPlainObject(source)) {
      return utils.merge({}, source);
    } else if (utils.isArray(source)) {
      return source.slice();
    }
    return source;
  }

  function mergeDeepProperties(prop) {
    if (!utils.isUndefined(config2[prop])) {
      config[prop] = getMergedValue(config1[prop], config2[prop]);
    } else if (!utils.isUndefined(config1[prop])) {
      config[prop] = getMergedValue(undefined, config1[prop]);
    }
  }

  utils.forEach(valueFromConfig2Keys, function valueFromConfig2(prop) {
    if (!utils.isUndefined(config2[prop])) {
      config[prop] = getMergedValue(undefined, config2[prop]);
    }
  });

  utils.forEach(mergeDeepPropertiesKeys, mergeDeepProperties);

  utils.forEach(defaultToConfig2Keys, function defaultToConfig2(prop) {
    if (!utils.isUndefined(config2[prop])) {
      config[prop] = getMergedValue(undefined, config2[prop]);
    } else if (!utils.isUndefined(config1[prop])) {
      config[prop] = getMergedValue(undefined, config1[prop]);
    }
  });

  utils.forEach(directMergeKeys, function merge(prop) {
    if (prop in config2) {
      config[prop] = getMergedValue(config1[prop], config2[prop]);
    } else if (prop in config1) {
      config[prop] = getMergedValue(undefined, config1[prop]);
    }
  });

  var axiosKeys = valueFromConfig2Keys
    .concat(mergeDeepPropertiesKeys)
    .concat(defaultToConfig2Keys)
    .concat(directMergeKeys);

  var otherKeys = Object
    .keys(config1)
    .concat(Object.keys(config2))
    .filter(function filterAxiosKeys(key) {
      return axiosKeys.indexOf(key) === -1;
    });

  utils.forEach(otherKeys, mergeDeepProperties);

  return config;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/*!***********************************************!*\
  !*** ./node_modules/axios/lib/core/settle.js ***!
  \***********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var createError = __webpack_require__(/*! ./createError */ "./node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/*!******************************************************!*\
  !*** ./node_modules/axios/lib/core/transformData.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");
var defaults = __webpack_require__(/*! ./../defaults */ "./node_modules/axios/lib/defaults.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  var context = this || defaults;
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn.call(context, data, headers);
  });

  return data;
};


/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/*!********************************************!*\
  !*** ./node_modules/axios/lib/defaults.js ***!
  \********************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";
/* provided dependency */ var process = __webpack_require__(/*! process/browser */ "./node_modules/process/browser.js");


var utils = __webpack_require__(/*! ./utils */ "./node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__(/*! ./helpers/normalizeHeaderName */ "./node_modules/axios/lib/helpers/normalizeHeaderName.js");
var enhanceError = __webpack_require__(/*! ./core/enhanceError */ "./node_modules/axios/lib/core/enhanceError.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__(/*! ./adapters/xhr */ "./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof process !== 'undefined' && Object.prototype.toString.call(process) === '[object process]') {
    // For node use HTTP adapter
    adapter = __webpack_require__(/*! ./adapters/http */ "./node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

function stringifySafely(rawValue, parser, encoder) {
  if (utils.isString(rawValue)) {
    try {
      (parser || JSON.parse)(rawValue);
      return utils.trim(rawValue);
    } catch (e) {
      if (e.name !== 'SyntaxError') {
        throw e;
      }
    }
  }

  return (encoder || JSON.stringify)(rawValue);
}

var defaults = {

  transitional: {
    silentJSONParsing: true,
    forcedJSONParsing: true,
    clarifyTimeoutError: false
  },

  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Accept');
    normalizeHeaderName(headers, 'Content-Type');

    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data) || (headers && headers['Content-Type'] === 'application/json')) {
      setContentTypeIfUnset(headers, 'application/json');
      return stringifySafely(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    var transitional = this.transitional;
    var silentJSONParsing = transitional && transitional.silentJSONParsing;
    var forcedJSONParsing = transitional && transitional.forcedJSONParsing;
    var strictJSONParsing = !silentJSONParsing && this.responseType === 'json';

    if (strictJSONParsing || (forcedJSONParsing && utils.isString(data) && data.length)) {
      try {
        return JSON.parse(data);
      } catch (e) {
        if (strictJSONParsing) {
          if (e.name === 'SyntaxError') {
            throw enhanceError(e, this, 'E_JSON_PARSE');
          }
          throw e;
        }
      }
    }

    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,
  maxBodyLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;


/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/*!************************************************!*\
  !*** ./node_modules/axios/lib/helpers/bind.js ***!
  \************************************************/
/***/ ((module) => {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/*!****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/buildURL.js ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');
    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/*!*******************************************************!*\
  !*** ./node_modules/axios/lib/helpers/combineURLs.js ***!
  \*******************************************************/
/***/ ((module) => {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/*!***************************************************!*\
  !*** ./node_modules/axios/lib/helpers/cookies.js ***!
  \***************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
    (function standardBrowserEnv() {
      return {
        write: function write(name, value, expires, path, domain, secure) {
          var cookie = [];
          cookie.push(name + '=' + encodeURIComponent(value));

          if (utils.isNumber(expires)) {
            cookie.push('expires=' + new Date(expires).toGMTString());
          }

          if (utils.isString(path)) {
            cookie.push('path=' + path);
          }

          if (utils.isString(domain)) {
            cookie.push('domain=' + domain);
          }

          if (secure === true) {
            cookie.push('secure');
          }

          document.cookie = cookie.join('; ');
        },

        read: function read(name) {
          var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
          return (match ? decodeURIComponent(match[3]) : null);
        },

        remove: function remove(name) {
          this.write(name, '', Date.now() - 86400000);
        }
      };
    })() :

  // Non standard browser env (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return {
        write: function write() {},
        read: function read() { return null; },
        remove: function remove() {}
      };
    })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/*!*********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAbsoluteURL.js ***!
  \*********************************************************/
/***/ ((module) => {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAxiosError.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isAxiosError.js ***!
  \********************************************************/
/***/ ((module) => {

"use strict";


/**
 * Determines whether the payload is an error thrown by Axios
 *
 * @param {*} payload The value to test
 * @returns {boolean} True if the payload is an error thrown by Axios, otherwise false
 */
module.exports = function isAxiosError(payload) {
  return (typeof payload === 'object') && (payload.isAxiosError === true);
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/*!***********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/isURLSameOrigin.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
    (function standardBrowserEnv() {
      var msie = /(msie|trident)/i.test(navigator.userAgent);
      var urlParsingNode = document.createElement('a');
      var originURL;

      /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
      function resolveURL(url) {
        var href = url;

        if (msie) {
        // IE needs attribute set twice to normalize properties
          urlParsingNode.setAttribute('href', href);
          href = urlParsingNode.href;
        }

        urlParsingNode.setAttribute('href', href);

        // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
        return {
          href: urlParsingNode.href,
          protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
          host: urlParsingNode.host,
          search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
          hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
          hostname: urlParsingNode.hostname,
          port: urlParsingNode.port,
          pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
            urlParsingNode.pathname :
            '/' + urlParsingNode.pathname
        };
      }

      originURL = resolveURL(window.location.href);

      /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
      return function isURLSameOrigin(requestURL) {
        var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
        return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
      };
    })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
    (function nonStandardBrowserEnv() {
      return function isURLSameOrigin() {
        return true;
      };
    })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/*!***************************************************************!*\
  !*** ./node_modules/axios/lib/helpers/normalizeHeaderName.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/*!********************************************************!*\
  !*** ./node_modules/axios/lib/helpers/parseHeaders.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var utils = __webpack_require__(/*! ./../utils */ "./node_modules/axios/lib/utils.js");

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/*!**************************************************!*\
  !*** ./node_modules/axios/lib/helpers/spread.js ***!
  \**************************************************/
/***/ ((module) => {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/validator.js":
/*!*****************************************************!*\
  !*** ./node_modules/axios/lib/helpers/validator.js ***!
  \*****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var pkg = __webpack_require__(/*! ./../../package.json */ "./node_modules/axios/package.json");

var validators = {};

// eslint-disable-next-line func-names
['object', 'boolean', 'number', 'function', 'string', 'symbol'].forEach(function(type, i) {
  validators[type] = function validator(thing) {
    return typeof thing === type || 'a' + (i < 1 ? 'n ' : ' ') + type;
  };
});

var deprecatedWarnings = {};
var currentVerArr = pkg.version.split('.');

/**
 * Compare package versions
 * @param {string} version
 * @param {string?} thanVersion
 * @returns {boolean}
 */
function isOlderVersion(version, thanVersion) {
  var pkgVersionArr = thanVersion ? thanVersion.split('.') : currentVerArr;
  var destVer = version.split('.');
  for (var i = 0; i < 3; i++) {
    if (pkgVersionArr[i] > destVer[i]) {
      return true;
    } else if (pkgVersionArr[i] < destVer[i]) {
      return false;
    }
  }
  return false;
}

/**
 * Transitional option validator
 * @param {function|boolean?} validator
 * @param {string?} version
 * @param {string} message
 * @returns {function}
 */
validators.transitional = function transitional(validator, version, message) {
  var isDeprecated = version && isOlderVersion(version);

  function formatMessage(opt, desc) {
    return '[Axios v' + pkg.version + '] Transitional option \'' + opt + '\'' + desc + (message ? '. ' + message : '');
  }

  // eslint-disable-next-line func-names
  return function(value, opt, opts) {
    if (validator === false) {
      throw new Error(formatMessage(opt, ' has been removed in ' + version));
    }

    if (isDeprecated && !deprecatedWarnings[opt]) {
      deprecatedWarnings[opt] = true;
      // eslint-disable-next-line no-console
      console.warn(
        formatMessage(
          opt,
          ' has been deprecated since v' + version + ' and will be removed in the near future'
        )
      );
    }

    return validator ? validator(value, opt, opts) : true;
  };
};

/**
 * Assert object's properties type
 * @param {object} options
 * @param {object} schema
 * @param {boolean?} allowUnknown
 */

function assertOptions(options, schema, allowUnknown) {
  if (typeof options !== 'object') {
    throw new TypeError('options must be an object');
  }
  var keys = Object.keys(options);
  var i = keys.length;
  while (i-- > 0) {
    var opt = keys[i];
    var validator = schema[opt];
    if (validator) {
      var value = options[opt];
      var result = value === undefined || validator(value, opt, options);
      if (result !== true) {
        throw new TypeError('option ' + opt + ' must be ' + result);
      }
      continue;
    }
    if (allowUnknown !== true) {
      throw Error('Unknown option ' + opt);
    }
  }
}

module.exports = {
  isOlderVersion: isOlderVersion,
  assertOptions: assertOptions,
  validators: validators
};


/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/*!*****************************************!*\
  !*** ./node_modules/axios/lib/utils.js ***!
  \*****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";


var bind = __webpack_require__(/*! ./helpers/bind */ "./node_modules/axios/lib/helpers/bind.js");

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is a Buffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Buffer, otherwise false
 */
function isBuffer(val) {
  return val !== null && !isUndefined(val) && val.constructor !== null && !isUndefined(val.constructor)
    && typeof val.constructor.isBuffer === 'function' && val.constructor.isBuffer(val);
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a plain Object
 *
 * @param {Object} val The value to test
 * @return {boolean} True if value is a plain Object, otherwise false
 */
function isPlainObject(val) {
  if (toString.call(val) !== '[object Object]') {
    return false;
  }

  var prototype = Object.getPrototypeOf(val);
  return prototype === null || prototype === Object.prototype;
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 * nativescript
 *  navigator.product -> 'NativeScript' or 'NS'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && (navigator.product === 'ReactNative' ||
                                           navigator.product === 'NativeScript' ||
                                           navigator.product === 'NS')) {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (isPlainObject(result[key]) && isPlainObject(val)) {
      result[key] = merge(result[key], val);
    } else if (isPlainObject(val)) {
      result[key] = merge({}, val);
    } else if (isArray(val)) {
      result[key] = val.slice();
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

/**
 * Remove byte order marker. This catches EF BB BF (the UTF-8 BOM)
 *
 * @param {string} content with BOM
 * @return {string} content value without BOM
 */
function stripBOM(content) {
  if (content.charCodeAt(0) === 0xFEFF) {
    content = content.slice(1);
  }
  return content;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isPlainObject: isPlainObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim,
  stripBOM: stripBOM
};


/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var _require = __webpack_require__(/*! axios */ "./node_modules/axios/index.js"),
    axios = _require["default"];

__webpack_require__(/*! ./tailwind-datepicker/js/datepicker-full */ "./resources/js/tailwind-datepicker/js/datepicker-full.js");

__webpack_require__(/*! ./tailwind-datepicker/js/locales/ka */ "./resources/js/tailwind-datepicker/js/locales/ka.js");

__webpack_require__(/*! axios */ "./node_modules/axios/index.js");

var datepicker_options = {
  format: "dd/mm/yyyy",
  language: "ka",
  autohide: true,
  prevArrow: '<svg class="h-8 w-8 text-blue-500" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke="none" d="M0 0h24v24H0z"/>  <polyline points="15 6 9 12 15 18" /></svg >',
  nextArrow: '<svg class="h-8 w-8 text-blue-500" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke="none" d="M0 0h24v24H0z"/>  <polyline points="9 6 15 12 9 18" /></svg >',
  maxDate: new Date()
};
var pariod_start = document.getElementById('period_start');
new Datepicker(pariod_start, datepicker_options);
var pariod_end = document.getElementById('period_end');
new Datepicker(pariod_end, datepicker_options);

window.resetform = function () {
  document.getElementById('findclaim').reset();
};

var submit_button = document.getElementById('submit_form');
submit_button.addEventListener('click', function (event) {
  var form = document.getElementById('findclaim');
  var formData = new FormData(form);
  axios.post('/api/search', formData).then(function (response) {
    var data = response.data;
    console.log(data);
  });
});

/***/ }),

/***/ "./resources/js/tailwind-datepicker/js/datepicker-full.js":
/*!****************************************************************!*\
  !*** ./resources/js/tailwind-datepicker/js/datepicker-full.js ***!
  \****************************************************************/
/***/ (() => {

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

(function () {
  'use strict';

  function hasProperty(obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  }

  function lastItemOf(arr) {
    return arr[arr.length - 1];
  } // push only the items not included in the array


  function pushUnique(arr) {
    for (var _len = arguments.length, items = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      items[_key - 1] = arguments[_key];
    }

    items.forEach(function (item) {
      if (arr.includes(item)) {
        return;
      }

      arr.push(item);
    });
    return arr;
  }

  function stringToArray(str, separator) {
    // convert empty string to an empty array
    return str ? str.split(separator) : [];
  }

  function isInRange(testVal, min, max) {
    var minOK = min === undefined || testVal >= min;
    var maxOK = max === undefined || testVal <= max;
    return minOK && maxOK;
  }

  function limitToRange(val, min, max) {
    if (val < min) {
      return min;
    }

    if (val > max) {
      return max;
    }

    return val;
  }

  function createTagRepeat(tagName, repeat) {
    var attributes = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var index = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
    var html = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '';
    var openTagSrc = Object.keys(attributes).reduce(function (src, attr) {
      var val = attributes[attr];

      if (typeof val === 'function') {
        val = val(index);
      }

      return "".concat(src, " ").concat(attr, "=\"").concat(val, "\"");
    }, tagName);
    html += "<".concat(openTagSrc, "></").concat(tagName, ">");
    var next = index + 1;
    return next < repeat ? createTagRepeat(tagName, repeat, attributes, next, html) : html;
  } // Remove the spacing surrounding tags for HTML parser not to create text nodes
  // before/after elements


  function optimizeTemplateHTML(html) {
    return html.replace(/>\s+/g, '>').replace(/\s+</, '<');
  }

  function stripTime(timeValue) {
    return new Date(timeValue).setHours(0, 0, 0, 0);
  }

  function today() {
    return new Date().setHours(0, 0, 0, 0);
  } // Get the time value of the start of given date or year, month and day


  function dateValue() {
    switch (arguments.length) {
      case 0:
        return today();

      case 1:
        return stripTime(arguments.length <= 0 ? undefined : arguments[0]);
    } // use setFullYear() to keep 2-digit year from being mapped to 1900-1999


    var newDate = new Date(0);
    newDate.setFullYear.apply(newDate, arguments);
    return newDate.setHours(0, 0, 0, 0);
  }

  function addDays(date, amount) {
    var newDate = new Date(date);
    return newDate.setDate(newDate.getDate() + amount);
  }

  function addWeeks(date, amount) {
    return addDays(date, amount * 7);
  }

  function addMonths(date, amount) {
    // If the day of the date is not in the new month, the last day of the new
    // month will be returned. e.g. Jan 31 + 1 month → Feb 28 (not Mar 03)
    var newDate = new Date(date);
    var monthsToSet = newDate.getMonth() + amount;
    var expectedMonth = monthsToSet % 12;

    if (expectedMonth < 0) {
      expectedMonth += 12;
    }

    var time = newDate.setMonth(monthsToSet);
    return newDate.getMonth() !== expectedMonth ? newDate.setDate(0) : time;
  }

  function addYears(date, amount) {
    // If the date is Feb 29 and the new year is not a leap year, Feb 28 of the
    // new year will be returned.
    var newDate = new Date(date);
    var expectedMonth = newDate.getMonth();
    var time = newDate.setFullYear(newDate.getFullYear() + amount);
    return expectedMonth === 1 && newDate.getMonth() === 2 ? newDate.setDate(0) : time;
  } // Calculate the distance bettwen 2 days of the week


  function dayDiff(day, from) {
    return (day - from + 7) % 7;
  } // Get the date of the specified day of the week of given base date


  function dayOfTheWeekOf(baseDate, dayOfWeek) {
    var weekStart = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    var baseDay = new Date(baseDate).getDay();
    return addDays(baseDate, dayDiff(dayOfWeek, weekStart) - dayDiff(baseDay, weekStart));
  } // Get the ISO week of a date


  function getWeek(date) {
    // start of ISO week is Monday
    var thuOfTheWeek = dayOfTheWeekOf(date, 4, 1); // 1st week == the week where the 4th of January is in

    var firstThu = dayOfTheWeekOf(new Date(thuOfTheWeek).setMonth(0, 4), 4, 1);
    return Math.round((thuOfTheWeek - firstThu) / 604800000) + 1;
  } // Get the start year of the period of years that includes given date
  // years: length of the year period


  function startOfYearPeriod(date, years) {
    /* @see https://en.wikipedia.org/wiki/Year_zero#ISO_8601 */
    var year = new Date(date).getFullYear();
    return Math.floor(year / years) * years;
  } // pattern for format parts


  var reFormatTokens = /dd?|DD?|mm?|MM?|yy?(?:yy)?/; // pattern for non date parts

  var reNonDateParts = /[\s!-/:-@[-`{-~年月日]+/; // cache for persed formats

  var knownFormats = {}; // parse funtions for date parts

  var parseFns = {
    y: function y(date, year) {
      return new Date(date).setFullYear(parseInt(year, 10));
    },
    m: function m(date, month, locale) {
      var newDate = new Date(date);
      var monthIndex = parseInt(month, 10) - 1;

      if (isNaN(monthIndex)) {
        if (!month) {
          return NaN;
        }

        var monthName = month.toLowerCase();

        var compareNames = function compareNames(name) {
          return name.toLowerCase().startsWith(monthName);
        }; // compare with both short and full names because some locales have periods
        // in the short names (not equal to the first X letters of the full names)


        monthIndex = locale.monthsShort.findIndex(compareNames);

        if (monthIndex < 0) {
          monthIndex = locale.months.findIndex(compareNames);
        }

        if (monthIndex < 0) {
          return NaN;
        }
      }

      newDate.setMonth(monthIndex);
      return newDate.getMonth() !== normalizeMonth(monthIndex) ? newDate.setDate(0) : newDate.getTime();
    },
    d: function d(date, day) {
      return new Date(date).setDate(parseInt(day, 10));
    }
  }; // format functions for date parts

  var formatFns = {
    d: function d(date) {
      return date.getDate();
    },
    dd: function dd(date) {
      return padZero(date.getDate(), 2);
    },
    D: function D(date, locale) {
      return locale.daysShort[date.getDay()];
    },
    DD: function DD(date, locale) {
      return locale.days[date.getDay()];
    },
    m: function m(date) {
      return date.getMonth() + 1;
    },
    mm: function mm(date) {
      return padZero(date.getMonth() + 1, 2);
    },
    M: function M(date, locale) {
      return locale.monthsShort[date.getMonth()];
    },
    MM: function MM(date, locale) {
      return locale.months[date.getMonth()];
    },
    y: function y(date) {
      return date.getFullYear();
    },
    yy: function yy(date) {
      return padZero(date.getFullYear(), 2).slice(-2);
    },
    yyyy: function yyyy(date) {
      return padZero(date.getFullYear(), 4);
    }
  }; // get month index in normal range (0 - 11) from any number

  function normalizeMonth(monthIndex) {
    return monthIndex > -1 ? monthIndex % 12 : normalizeMonth(monthIndex + 12);
  }

  function padZero(num, length) {
    return num.toString().padStart(length, '0');
  }

  function parseFormatString(format) {
    if (typeof format !== 'string') {
      throw new Error("Invalid date format.");
    }

    if (format in knownFormats) {
      return knownFormats[format];
    } // sprit the format string into parts and seprators


    var separators = format.split(reFormatTokens);
    var parts = format.match(new RegExp(reFormatTokens, 'g'));

    if (separators.length === 0 || !parts) {
      throw new Error("Invalid date format.");
    } // collect format functions used in the format


    var partFormatters = parts.map(function (token) {
      return formatFns[token];
    }); // collect parse function keys used in the format
    // iterate over parseFns' keys in order to keep the order of the keys.

    var partParserKeys = Object.keys(parseFns).reduce(function (keys, key) {
      var token = parts.find(function (part) {
        return part[0] !== 'D' && part[0].toLowerCase() === key;
      });

      if (token) {
        keys.push(key);
      }

      return keys;
    }, []);
    return knownFormats[format] = {
      parser: function parser(dateStr, locale) {
        var dateParts = dateStr.split(reNonDateParts).reduce(function (dtParts, part, index) {
          if (part.length > 0 && parts[index]) {
            var token = parts[index][0];

            if (token === 'M') {
              dtParts.m = part;
            } else if (token !== 'D') {
              dtParts[token] = part;
            }
          }

          return dtParts;
        }, {}); // iterate over partParserkeys so that the parsing is made in the oder
        // of year, month and day to prevent the day parser from correcting last
        // day of month wrongly

        return partParserKeys.reduce(function (origDate, key) {
          var newDate = parseFns[key](origDate, dateParts[key], locale); // ingnore the part failed to parse

          return isNaN(newDate) ? origDate : newDate;
        }, today());
      },
      formatter: function formatter(date, locale) {
        var dateStr = partFormatters.reduce(function (str, fn, index) {
          return str += "".concat(separators[index]).concat(fn(date, locale));
        }, ''); // separators' length is always parts' length + 1,

        return dateStr += lastItemOf(separators);
      }
    };
  }

  function _parseDate(dateStr, format, locale) {
    if (dateStr instanceof Date || typeof dateStr === 'number') {
      var date = stripTime(dateStr);
      return isNaN(date) ? undefined : date;
    }

    if (!dateStr) {
      return undefined;
    }

    if (dateStr === 'today') {
      return today();
    }

    if (format && format.toValue) {
      var _date = format.toValue(dateStr, format, locale);

      return isNaN(_date) ? undefined : stripTime(_date);
    }

    return parseFormatString(format).parser(dateStr, locale);
  }

  function _formatDate(date, format, locale) {
    if (isNaN(date) || !date && date !== 0) {
      return '';
    }

    var dateObj = typeof date === 'number' ? new Date(date) : date;

    if (format.toDisplay) {
      return format.toDisplay(dateObj, format, locale);
    }

    return parseFormatString(format).formatter(dateObj, locale);
  }

  var listenerRegistry = new WeakMap();
  var _EventTarget$prototyp = EventTarget.prototype,
      addEventListener = _EventTarget$prototyp.addEventListener,
      removeEventListener = _EventTarget$prototyp.removeEventListener; // Register event listeners to a key object
  // listeners: array of listener definitions;
  //   - each definition must be a flat array of event target and the arguments
  //     used to call addEventListener() on the target

  function registerListeners(keyObj, listeners) {
    var registered = listenerRegistry.get(keyObj);

    if (!registered) {
      registered = [];
      listenerRegistry.set(keyObj, registered);
    }

    listeners.forEach(function (listener) {
      addEventListener.call.apply(addEventListener, _toConsumableArray(listener));
      registered.push(listener);
    });
  }

  function unregisterListeners(keyObj) {
    var listeners = listenerRegistry.get(keyObj);

    if (!listeners) {
      return;
    }

    listeners.forEach(function (listener) {
      removeEventListener.call.apply(removeEventListener, _toConsumableArray(listener));
    });
    listenerRegistry["delete"](keyObj);
  } // Event.composedPath() polyfill for Edge
  // based on https://gist.github.com/kleinfreund/e9787d73776c0e3750dcfcdc89f100ec


  if (!Event.prototype.composedPath) {
    var getComposedPath = function getComposedPath(node) {
      var path = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      path.push(node);
      var parent;

      if (node.parentNode) {
        parent = node.parentNode;
      } else if (node.host) {
        // ShadowRoot
        parent = node.host;
      } else if (node.defaultView) {
        // Document
        parent = node.defaultView;
      }

      return parent ? getComposedPath(parent, path) : path;
    };

    Event.prototype.composedPath = function () {
      return getComposedPath(this.target);
    };
  }

  function findFromPath(path, criteria, currentTarget) {
    var index = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
    var el = path[index];

    if (criteria(el)) {
      return el;
    } else if (el === currentTarget || !el.parentElement) {
      // stop when reaching currentTarget or <html>
      return;
    }

    return findFromPath(path, criteria, currentTarget, index + 1);
  } // Search for the actual target of a delegated event


  function findElementInEventPath(ev, selector) {
    var criteria = typeof selector === 'function' ? selector : function (el) {
      return el.matches(selector);
    };
    return findFromPath(ev.composedPath(), criteria, ev.currentTarget);
  } // default locales


  var locales = {
    en: {
      days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      today: "Today",
      clear: "Clear",
      titleFormat: "MM y"
    }
  }; // config options updatable by setOptions() and their default values

  var defaultOptions = {
    autohide: false,
    beforeShowDay: null,
    beforeShowDecade: null,
    beforeShowMonth: null,
    beforeShowYear: null,
    calendarWeeks: false,
    clearBtn: false,
    dateDelimiter: ',',
    datesDisabled: [],
    daysOfWeekDisabled: [],
    daysOfWeekHighlighted: [],
    defaultViewDate: undefined,
    // placeholder, defaults to today() by the program
    disableTouchKeyboard: false,
    format: 'mm/dd/yyyy',
    language: 'en',
    maxDate: null,
    maxNumberOfDates: 1,
    maxView: 3,
    minDate: null,
    nextArrow: '<svg class="w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>',
    orientation: 'auto',
    pickLevel: 0,
    prevArrow: '<svg class="w-4 h-4" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M9.707 16.707a1 1 0 01-1.414 0l-6-6a1 1 0 010-1.414l6-6a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l4.293 4.293a1 1 0 010 1.414z" clip-rule="evenodd"></path></svg>',
    showDaysOfWeek: true,
    showOnClick: true,
    showOnFocus: true,
    startView: 0,
    title: '',
    todayBtn: false,
    todayBtnMode: 0,
    todayHighlight: false,
    updateOnBlur: true,
    weekStart: 0
  };
  var range = document.createRange();

  function parseHTML(html) {
    return range.createContextualFragment(html);
  }

  function hideElement(el) {
    if (el.style.display === 'none') {
      return;
    } // back up the existing display setting in data-style-display


    if (el.style.display) {
      el.dataset.styleDisplay = el.style.display;
    }

    el.style.display = 'none';
  }

  function showElement(el) {
    if (el.style.display !== 'none') {
      return;
    }

    if (el.dataset.styleDisplay) {
      // restore backed-up dispay property
      el.style.display = el.dataset.styleDisplay;
      delete el.dataset.styleDisplay;
    } else {
      el.style.display = '';
    }
  }

  function emptyChildNodes(el) {
    if (el.firstChild) {
      el.removeChild(el.firstChild);
      emptyChildNodes(el);
    }
  }

  function replaceChildNodes(el, newChildNodes) {
    emptyChildNodes(el);

    if (newChildNodes instanceof DocumentFragment) {
      el.appendChild(newChildNodes);
    } else if (typeof newChildNodes === 'string') {
      el.appendChild(parseHTML(newChildNodes));
    } else if (typeof newChildNodes.forEach === 'function') {
      newChildNodes.forEach(function (node) {
        el.appendChild(node);
      });
    }
  }

  var defaultLang = defaultOptions.language,
      defaultFormat = defaultOptions.format,
      defaultWeekStart = defaultOptions.weekStart; // Reducer function to filter out invalid day-of-week from the input

  function sanitizeDOW(dow, day) {
    return dow.length < 6 && day >= 0 && day < 7 ? pushUnique(dow, day) : dow;
  }

  function calcEndOfWeek(startOfWeek) {
    return (startOfWeek + 6) % 7;
  } // validate input date. if invalid, fallback to the original value


  function validateDate(value, format, locale, origValue) {
    var date = _parseDate(value, format, locale);

    return date !== undefined ? date : origValue;
  } // Validate viewId. if invalid, fallback to the original value


  function validateViewId(value, origValue) {
    var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3;
    var viewId = parseInt(value, 10);
    return viewId >= 0 && viewId <= max ? viewId : origValue;
  } // Create Datepicker configuration to set


  function processOptions(options, datepicker) {
    var inOpts = Object.assign({}, options);
    var config = {};
    var locales = datepicker.constructor.locales;

    var _ref = datepicker.config || {},
        format = _ref.format,
        language = _ref.language,
        locale = _ref.locale,
        maxDate = _ref.maxDate,
        maxView = _ref.maxView,
        minDate = _ref.minDate,
        pickLevel = _ref.pickLevel,
        startView = _ref.startView,
        weekStart = _ref.weekStart;

    if (inOpts.language) {
      var lang;

      if (inOpts.language !== language) {
        if (locales[inOpts.language]) {
          lang = inOpts.language;
        } else {
          // Check if langauge + region tag can fallback to the one without
          // region (e.g. fr-CA → fr)
          lang = inOpts.language.split('-')[0];

          if (locales[lang] === undefined) {
            lang = false;
          }
        }
      }

      delete inOpts.language;

      if (lang) {
        language = config.language = lang; // update locale as well when updating language

        var origLocale = locale || locales[defaultLang]; // use default language's properties for the fallback

        locale = Object.assign({
          format: defaultFormat,
          weekStart: defaultWeekStart
        }, locales[defaultLang]);

        if (language !== defaultLang) {
          Object.assign(locale, locales[language]);
        }

        config.locale = locale; // if format and/or weekStart are the same as old locale's defaults,
        // update them to new locale's defaults

        if (format === origLocale.format) {
          format = config.format = locale.format;
        }

        if (weekStart === origLocale.weekStart) {
          weekStart = config.weekStart = locale.weekStart;
          config.weekEnd = calcEndOfWeek(locale.weekStart);
        }
      }
    }

    if (inOpts.format) {
      var hasToDisplay = typeof inOpts.format.toDisplay === 'function';
      var hasToValue = typeof inOpts.format.toValue === 'function';
      var validFormatString = reFormatTokens.test(inOpts.format);

      if (hasToDisplay && hasToValue || validFormatString) {
        format = config.format = inOpts.format;
      }

      delete inOpts.format;
    } //*** dates ***//
    // while min and maxDate for "no limit" in the options are better to be null
    // (especially when updating), the ones in the config have to be undefined
    // because null is treated as 0 (= unix epoch) when comparing with time value


    var minDt = minDate;
    var maxDt = maxDate;

    if (inOpts.minDate !== undefined) {
      minDt = inOpts.minDate === null ? dateValue(0, 0, 1) // set 0000-01-01 to prevent negative values for year
      : validateDate(inOpts.minDate, format, locale, minDt);
      delete inOpts.minDate;
    }

    if (inOpts.maxDate !== undefined) {
      maxDt = inOpts.maxDate === null ? undefined : validateDate(inOpts.maxDate, format, locale, maxDt);
      delete inOpts.maxDate;
    }

    if (maxDt < minDt) {
      minDate = config.minDate = maxDt;
      maxDate = config.maxDate = minDt;
    } else {
      if (minDate !== minDt) {
        minDate = config.minDate = minDt;
      }

      if (maxDate !== maxDt) {
        maxDate = config.maxDate = maxDt;
      }
    }

    if (inOpts.datesDisabled) {
      config.datesDisabled = inOpts.datesDisabled.reduce(function (dates, dt) {
        var date = _parseDate(dt, format, locale);

        return date !== undefined ? pushUnique(dates, date) : dates;
      }, []);
      delete inOpts.datesDisabled;
    }

    if (inOpts.defaultViewDate !== undefined) {
      var viewDate = _parseDate(inOpts.defaultViewDate, format, locale);

      if (viewDate !== undefined) {
        config.defaultViewDate = viewDate;
      }

      delete inOpts.defaultViewDate;
    } //*** days of week ***//


    if (inOpts.weekStart !== undefined) {
      var wkStart = Number(inOpts.weekStart) % 7;

      if (!isNaN(wkStart)) {
        weekStart = config.weekStart = wkStart;
        config.weekEnd = calcEndOfWeek(wkStart);
      }

      delete inOpts.weekStart;
    }

    if (inOpts.daysOfWeekDisabled) {
      config.daysOfWeekDisabled = inOpts.daysOfWeekDisabled.reduce(sanitizeDOW, []);
      delete inOpts.daysOfWeekDisabled;
    }

    if (inOpts.daysOfWeekHighlighted) {
      config.daysOfWeekHighlighted = inOpts.daysOfWeekHighlighted.reduce(sanitizeDOW, []);
      delete inOpts.daysOfWeekHighlighted;
    } //*** multi date ***//


    if (inOpts.maxNumberOfDates !== undefined) {
      var maxNumberOfDates = parseInt(inOpts.maxNumberOfDates, 10);

      if (maxNumberOfDates >= 0) {
        config.maxNumberOfDates = maxNumberOfDates;
        config.multidate = maxNumberOfDates !== 1;
      }

      delete inOpts.maxNumberOfDates;
    }

    if (inOpts.dateDelimiter) {
      config.dateDelimiter = String(inOpts.dateDelimiter);
      delete inOpts.dateDelimiter;
    } //*** pick level & view ***//


    var newPickLevel = pickLevel;

    if (inOpts.pickLevel !== undefined) {
      newPickLevel = validateViewId(inOpts.pickLevel, 2);
      delete inOpts.pickLevel;
    }

    if (newPickLevel !== pickLevel) {
      pickLevel = config.pickLevel = newPickLevel;
    }

    var newMaxView = maxView;

    if (inOpts.maxView !== undefined) {
      newMaxView = validateViewId(inOpts.maxView, maxView);
      delete inOpts.maxView;
    } // ensure max view >= pick level


    newMaxView = pickLevel > newMaxView ? pickLevel : newMaxView;

    if (newMaxView !== maxView) {
      maxView = config.maxView = newMaxView;
    }

    var newStartView = startView;

    if (inOpts.startView !== undefined) {
      newStartView = validateViewId(inOpts.startView, newStartView);
      delete inOpts.startView;
    } // ensure pick level <= start view <= max view


    if (newStartView < pickLevel) {
      newStartView = pickLevel;
    } else if (newStartView > maxView) {
      newStartView = maxView;
    }

    if (newStartView !== startView) {
      config.startView = newStartView;
    } //*** template ***//


    if (inOpts.prevArrow) {
      var prevArrow = parseHTML(inOpts.prevArrow);

      if (prevArrow.childNodes.length > 0) {
        config.prevArrow = prevArrow.childNodes;
      }

      delete inOpts.prevArrow;
    }

    if (inOpts.nextArrow) {
      var nextArrow = parseHTML(inOpts.nextArrow);

      if (nextArrow.childNodes.length > 0) {
        config.nextArrow = nextArrow.childNodes;
      }

      delete inOpts.nextArrow;
    } //*** misc ***//


    if (inOpts.disableTouchKeyboard !== undefined) {
      config.disableTouchKeyboard = 'ontouchstart' in document && !!inOpts.disableTouchKeyboard;
      delete inOpts.disableTouchKeyboard;
    }

    if (inOpts.orientation) {
      var orientation = inOpts.orientation.toLowerCase().split(/\s+/g);
      config.orientation = {
        x: orientation.find(function (x) {
          return x === 'left' || x === 'right';
        }) || 'auto',
        y: orientation.find(function (y) {
          return y === 'top' || y === 'bottom';
        }) || 'auto'
      };
      delete inOpts.orientation;
    }

    if (inOpts.todayBtnMode !== undefined) {
      switch (inOpts.todayBtnMode) {
        case 0:
        case 1:
          config.todayBtnMode = inOpts.todayBtnMode;
      }

      delete inOpts.todayBtnMode;
    } //*** copy the rest ***//


    Object.keys(inOpts).forEach(function (key) {
      if (inOpts[key] !== undefined && hasProperty(defaultOptions, key)) {
        config[key] = inOpts[key];
      }
    });
    return config;
  }

  var pickerTemplate = optimizeTemplateHTML("<div class=\"datepicker hidden\">\n  <div class=\"datepicker-picker inline-block rounded-lg bg-white shadow-lg p-4\">\n    <div class=\"datepicker-header\">\n      <div class=\"datepicker-title bg-white px-2 py-3 text-center font-semibold\"></div>\n      <div class=\"datepicker-controls flex justify-between mb-2\">\n        <button type=\"button\" class=\"bg-white rounded-lg text-gray-500 hover:bg-gray-100 hover:text-gray-900 text-lg p-2.5 focus:outline-none focus:ring-2 focus:ring-gray-200 prev-btn\"></button>\n        <button type=\"button\" class=\"text-sm rounded-lg text-gray-900 bg-white font-semibold py-2.5 px-5 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 view-switch\"></button>\n        <button type=\"button\" class=\"bg-white rounded-lg text-gray-500 hover:bg-gray-100 hover:text-gray-900 text-lg p-2.5 focus:outline-none focus:ring-2 focus:ring-gray-200 next-btn\"></button>\n      </div>\n    </div>\n    <div class=\"datepicker-main p-1\"></div>\n    <div class=\"datepicker-footer\">\n      <div class=\"datepicker-controls flex space-x-2 mt-2\">\n        <button type=\"button\" class=\"%buttonClass% today-btn text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2 text-center w-1/2\"></button>\n        <button type=\"button\" class=\"%buttonClass% clear-btn text-gray-900 bg-white border border-gray-300 hover:bg-gray-100 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2 text-center w-1/2\"></button>\n      </div>\n    </div>\n  </div>\n</div>");
  var daysTemplate = optimizeTemplateHTML("<div class=\"days\">\n  <div class=\"days-of-week grid grid-cols-7 mb-1\">".concat(createTagRepeat('span', 7, {
    "class": 'dow block flex-1 leading-9 border-0 rounded-lg cursor-default text-center text-gray-900 font-semibold text-sm'
  }), "</div>\n  <div class=\"datepicker-grid w-64 grid grid-cols-7\">").concat(createTagRepeat('span', 42, {
    "class": 'block flex-1 leading-9 border-0 rounded-lg cursor-default text-center text-gray-900 font-semibold text-sm h-6 leading-6 text-sm font-medium text-gray-500'
  }), "</div>\n</div>"));
  var calendarWeeksTemplate = optimizeTemplateHTML("<div class=\"calendar-weeks\">\n  <div class=\"days-of-week flex\"><span class=\"dow h-6 leading-6 text-sm font-medium text-gray-500\"></span></div>\n  <div class=\"weeks\">".concat(createTagRepeat('span', 6, {
    "class": 'week block flex-1 leading-9 border-0 rounded-lg cursor-default text-center text-gray-900 font-semibold text-sm'
  }), "</div>\n</div>")); // Base class of the view classes

  var View = /*#__PURE__*/function () {
    function View(picker, config) {
      _classCallCheck(this, View);

      Object.assign(this, config, {
        picker: picker,
        element: parseHTML("<div class=\"datepicker-view flex\"></div>").firstChild,
        selected: []
      });
      this.init(this.picker.datepicker.config);
    }

    _createClass(View, [{
      key: "init",
      value: function init(options) {
        if (options.pickLevel !== undefined) {
          this.isMinView = this.id === options.pickLevel;
        }

        this.setOptions(options);
        this.updateFocus();
        this.updateSelection();
      } // Execute beforeShow() callback and apply the result to the element
      // args:
      // - current - current value on the iteration on view rendering
      // - timeValue - time value of the date to pass to beforeShow()

    }, {
      key: "performBeforeHook",
      value: function performBeforeHook(el, current, timeValue) {
        var result = this.beforeShow(new Date(timeValue));

        switch (_typeof(result)) {
          case 'boolean':
            result = {
              enabled: result
            };
            break;

          case 'string':
            result = {
              classes: result
            };
        }

        if (result) {
          if (result.enabled === false) {
            el.classList.add('disabled');
            pushUnique(this.disabled, current);
          }

          if (result.classes) {
            var _el$classList;

            var extraClasses = result.classes.split(/\s+/);

            (_el$classList = el.classList).add.apply(_el$classList, _toConsumableArray(extraClasses));

            if (extraClasses.includes('disabled')) {
              pushUnique(this.disabled, current);
            }
          }

          if (result.content) {
            replaceChildNodes(el, result.content);
          }
        }
      }
    }]);

    return View;
  }();

  var DaysView = /*#__PURE__*/function (_View) {
    _inherits(DaysView, _View);

    var _super = _createSuper(DaysView);

    function DaysView(picker) {
      _classCallCheck(this, DaysView);

      return _super.call(this, picker, {
        id: 0,
        name: 'days',
        cellClass: 'day'
      });
    }

    _createClass(DaysView, [{
      key: "init",
      value: function init(options) {
        var onConstruction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        if (onConstruction) {
          var inner = parseHTML(daysTemplate).firstChild;
          this.dow = inner.firstChild;
          this.grid = inner.lastChild;
          this.element.appendChild(inner);
        }

        _get(_getPrototypeOf(DaysView.prototype), "init", this).call(this, options);
      }
    }, {
      key: "setOptions",
      value: function setOptions(options) {
        var _this = this;

        var updateDOW;

        if (hasProperty(options, 'minDate')) {
          this.minDate = options.minDate;
        }

        if (hasProperty(options, 'maxDate')) {
          this.maxDate = options.maxDate;
        }

        if (options.datesDisabled) {
          this.datesDisabled = options.datesDisabled;
        }

        if (options.daysOfWeekDisabled) {
          this.daysOfWeekDisabled = options.daysOfWeekDisabled;
          updateDOW = true;
        }

        if (options.daysOfWeekHighlighted) {
          this.daysOfWeekHighlighted = options.daysOfWeekHighlighted;
        }

        if (options.todayHighlight !== undefined) {
          this.todayHighlight = options.todayHighlight;
        }

        if (options.weekStart !== undefined) {
          this.weekStart = options.weekStart;
          this.weekEnd = options.weekEnd;
          updateDOW = true;
        }

        if (options.locale) {
          var locale = this.locale = options.locale;
          this.dayNames = locale.daysMin;
          this.switchLabelFormat = locale.titleFormat;
          updateDOW = true;
        }

        if (options.beforeShowDay !== undefined) {
          this.beforeShow = typeof options.beforeShowDay === 'function' ? options.beforeShowDay : undefined;
        }

        if (options.calendarWeeks !== undefined) {
          if (options.calendarWeeks && !this.calendarWeeks) {
            var weeksElem = parseHTML(calendarWeeksTemplate).firstChild;
            this.calendarWeeks = {
              element: weeksElem,
              dow: weeksElem.firstChild,
              weeks: weeksElem.lastChild
            };
            this.element.insertBefore(weeksElem, this.element.firstChild);
          } else if (this.calendarWeeks && !options.calendarWeeks) {
            this.element.removeChild(this.calendarWeeks.element);
            this.calendarWeeks = null;
          }
        }

        if (options.showDaysOfWeek !== undefined) {
          if (options.showDaysOfWeek) {
            showElement(this.dow);

            if (this.calendarWeeks) {
              showElement(this.calendarWeeks.dow);
            }
          } else {
            hideElement(this.dow);

            if (this.calendarWeeks) {
              hideElement(this.calendarWeeks.dow);
            }
          }
        } // update days-of-week when locale, daysOfweekDisabled or weekStart is changed


        if (updateDOW) {
          Array.from(this.dow.children).forEach(function (el, index) {
            var dow = (_this.weekStart + index) % 7;
            el.textContent = _this.dayNames[dow];
            el.className = _this.daysOfWeekDisabled.includes(dow) ? 'dow disabled text-center h-6 leading-6 text-sm font-medium text-gray-500 cursor-not-allowed' : 'dow text-center h-6 leading-6 text-sm font-medium text-gray-500';
          });
        }
      } // Apply update on the focused date to view's settings

    }, {
      key: "updateFocus",
      value: function updateFocus() {
        var viewDate = new Date(this.picker.viewDate);
        var viewYear = viewDate.getFullYear();
        var viewMonth = viewDate.getMonth();
        var firstOfMonth = dateValue(viewYear, viewMonth, 1);
        var start = dayOfTheWeekOf(firstOfMonth, this.weekStart, this.weekStart);
        this.first = firstOfMonth;
        this.last = dateValue(viewYear, viewMonth + 1, 0);
        this.start = start;
        this.focused = this.picker.viewDate;
      } // Apply update on the selected dates to view's settings

    }, {
      key: "updateSelection",
      value: function updateSelection() {
        var _this$picker$datepick = this.picker.datepicker,
            dates = _this$picker$datepick.dates,
            rangepicker = _this$picker$datepick.rangepicker;
        this.selected = dates;

        if (rangepicker) {
          this.range = rangepicker.dates;
        }
      } // Update the entire view UI

    }, {
      key: "render",
      value: function render() {
        var _this2 = this;

        // update today marker on ever render
        this.today = this.todayHighlight ? today() : undefined; // refresh disabled dates on every render in order to clear the ones added
        // by beforeShow hook at previous render

        this.disabled = _toConsumableArray(this.datesDisabled);

        var switchLabel = _formatDate(this.focused, this.switchLabelFormat, this.locale);

        this.picker.setViewSwitchLabel(switchLabel);
        this.picker.setPrevBtnDisabled(this.first <= this.minDate);
        this.picker.setNextBtnDisabled(this.last >= this.maxDate);

        if (this.calendarWeeks) {
          // start of the UTC week (Monday) of the 1st of the month
          var startOfWeek = dayOfTheWeekOf(this.first, 1, 1);
          Array.from(this.calendarWeeks.weeks.children).forEach(function (el, index) {
            el.textContent = getWeek(addWeeks(startOfWeek, index));
          });
        }

        Array.from(this.grid.children).forEach(function (el, index) {
          var classList = el.classList;
          var current = addDays(_this2.start, index);
          var date = new Date(current);
          var day = date.getDay();
          el.className = "datepicker-cell hover:bg-gray-100 block flex-1 leading-9 border-0 rounded-lg cursor-pointer text-center text-gray-900 font-semibold text-sm ".concat(_this2.cellClass);
          el.dataset.date = current;
          el.textContent = date.getDate();

          if (current < _this2.first) {
            classList.add('prev', 'text-gray-500');
          } else if (current > _this2.last) {
            classList.add('next', 'text-gray-500');
          }

          if (_this2.today === current) {
            classList.add('today', 'bg-gray-100');
          }

          if (current < _this2.minDate || current > _this2.maxDate || _this2.disabled.includes(current)) {
            classList.add('disabled', 'cursor-not-allowed');
          }

          if (_this2.daysOfWeekDisabled.includes(day)) {
            classList.add('disabled', 'cursor-not-allowed');
            pushUnique(_this2.disabled, current);
          }

          if (_this2.daysOfWeekHighlighted.includes(day)) {
            classList.add('highlighted');
          }

          if (_this2.range) {
            var _this2$range = _slicedToArray(_this2.range, 2),
                rangeStart = _this2$range[0],
                rangeEnd = _this2$range[1];

            if (current > rangeStart && current < rangeEnd) {
              classList.add('range', 'bg-gray-200');
              classList.remove('rounded-lg', 'rounded-l-lg', 'rounded-r-lg');
            }

            if (current === rangeStart) {
              classList.add('range-start', 'bg-gray-100', 'rounded-l-lg');
              classList.remove('rounded-lg', 'rounded-r-lg');
            }

            if (current === rangeEnd) {
              classList.add('range-end', 'bg-gray-100', 'rounded-r-lg');
              classList.remove('rounded-lg', 'rounded-l-lg');
            }
          }

          if (_this2.selected.includes(current)) {
            classList.add('selected', 'bg-blue-500', 'text-white');
            classList.remove('text-gray-900', 'text-gray-500', 'hover:bg-gray-100');
          }

          if (current === _this2.focused) {
            classList.add('focused');
          }

          if (_this2.beforeShow) {
            _this2.performBeforeHook(el, current, current);
          }
        });
      } // Update the view UI by applying the changes of selected and focused items

    }, {
      key: "refresh",
      value: function refresh() {
        var _this3 = this;

        var _ref2 = this.range || [],
            _ref3 = _slicedToArray(_ref2, 2),
            rangeStart = _ref3[0],
            rangeEnd = _ref3[1];

        this.grid.querySelectorAll('.range, .range-start, .range-end, .selected, .focused').forEach(function (el) {
          el.classList.remove('range', 'range-start', 'range-end', 'selected', 'bg-blue-500', 'text-white', 'focused', 'bg-gray-100');
          el.classList.add('text-gray-900', 'rounded-lg');
        });
        Array.from(this.grid.children).forEach(function (el) {
          var current = Number(el.dataset.date);
          var classList = el.classList;

          if (current > rangeStart && current < rangeEnd) {
            classList.add('range', 'bg-gray-200');
            classList.remove('rounded-lg');
          }

          if (current === rangeStart) {
            classList.add('range-start', 'bg-gray-200', 'rounded-l-lg');
            classList.remove('rounded-lg', 'rounded-r-lg');
          }

          if (current === rangeEnd) {
            classList.add('range-end', 'bg-gray-200', 'rounded-r-lg');
            classList.remove('rounded-lg', 'rounded-l-lg');
          }

          if (_this3.selected.includes(current)) {
            classList.add('selected', 'bg-blue-500', 'text-white');
            classList.remove('text-gray-900', 'hover:bg-gray-100');
          }

          if (current === _this3.focused) {
            classList.add('focused', 'bg-gray-100');
          }
        });
      } // Update the view UI by applying the change of focused item

    }, {
      key: "refreshFocus",
      value: function refreshFocus() {
        var index = Math.round((this.focused - this.start) / 86400000);
        this.grid.querySelectorAll('.focused').forEach(function (el) {
          el.classList.remove('focused', 'bg-gray-100');
        });
        this.grid.children[index].classList.add('focused', 'bg-gray-100');
      }
    }]);

    return DaysView;
  }(View);

  function computeMonthRange(range, thisYear) {
    if (!range || !range[0] || !range[1]) {
      return;
    }

    var _range = _slicedToArray(range, 2),
        _range$ = _slicedToArray(_range[0], 2),
        startY = _range$[0],
        startM = _range$[1],
        _range$2 = _slicedToArray(_range[1], 2),
        endY = _range$2[0],
        endM = _range$2[1];

    if (startY > thisYear || endY < thisYear) {
      return;
    }

    return [startY === thisYear ? startM : -1, endY === thisYear ? endM : 12];
  }

  var MonthsView = /*#__PURE__*/function (_View2) {
    _inherits(MonthsView, _View2);

    var _super2 = _createSuper(MonthsView);

    function MonthsView(picker) {
      _classCallCheck(this, MonthsView);

      return _super2.call(this, picker, {
        id: 1,
        name: 'months',
        cellClass: 'month'
      });
    }

    _createClass(MonthsView, [{
      key: "init",
      value: function init(options) {
        var onConstruction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        if (onConstruction) {
          this.grid = this.element;
          this.element.classList.add('months', 'datepicker-grid', 'w-64', 'grid', 'grid-cols-4');
          this.grid.appendChild(parseHTML(createTagRepeat('span', 12, {
            'data-month': function dataMonth(ix) {
              return ix;
            }
          })));
        }

        _get(_getPrototypeOf(MonthsView.prototype), "init", this).call(this, options);
      }
    }, {
      key: "setOptions",
      value: function setOptions(options) {
        if (options.locale) {
          this.monthNames = options.locale.monthsShort;
        }

        if (hasProperty(options, 'minDate')) {
          if (options.minDate === undefined) {
            this.minYear = this.minMonth = this.minDate = undefined;
          } else {
            var minDateObj = new Date(options.minDate);
            this.minYear = minDateObj.getFullYear();
            this.minMonth = minDateObj.getMonth();
            this.minDate = minDateObj.setDate(1);
          }
        }

        if (hasProperty(options, 'maxDate')) {
          if (options.maxDate === undefined) {
            this.maxYear = this.maxMonth = this.maxDate = undefined;
          } else {
            var maxDateObj = new Date(options.maxDate);
            this.maxYear = maxDateObj.getFullYear();
            this.maxMonth = maxDateObj.getMonth();
            this.maxDate = dateValue(this.maxYear, this.maxMonth + 1, 0);
          }
        }

        if (options.beforeShowMonth !== undefined) {
          this.beforeShow = typeof options.beforeShowMonth === 'function' ? options.beforeShowMonth : undefined;
        }
      } // Update view's settings to reflect the viewDate set on the picker

    }, {
      key: "updateFocus",
      value: function updateFocus() {
        var viewDate = new Date(this.picker.viewDate);
        this.year = viewDate.getFullYear();
        this.focused = viewDate.getMonth();
      } // Update view's settings to reflect the selected dates

    }, {
      key: "updateSelection",
      value: function updateSelection() {
        var _this$picker$datepick2 = this.picker.datepicker,
            dates = _this$picker$datepick2.dates,
            rangepicker = _this$picker$datepick2.rangepicker;
        this.selected = dates.reduce(function (selected, timeValue) {
          var date = new Date(timeValue);
          var year = date.getFullYear();
          var month = date.getMonth();

          if (selected[year] === undefined) {
            selected[year] = [month];
          } else {
            pushUnique(selected[year], month);
          }

          return selected;
        }, {});

        if (rangepicker && rangepicker.dates) {
          this.range = rangepicker.dates.map(function (timeValue) {
            var date = new Date(timeValue);
            return isNaN(date) ? undefined : [date.getFullYear(), date.getMonth()];
          });
        }
      } // Update the entire view UI

    }, {
      key: "render",
      value: function render() {
        var _this4 = this;

        // refresh disabled months on every render in order to clear the ones added
        // by beforeShow hook at previous render
        this.disabled = [];
        this.picker.setViewSwitchLabel(this.year);
        this.picker.setPrevBtnDisabled(this.year <= this.minYear);
        this.picker.setNextBtnDisabled(this.year >= this.maxYear);
        var selected = this.selected[this.year] || [];
        var yrOutOfRange = this.year < this.minYear || this.year > this.maxYear;
        var isMinYear = this.year === this.minYear;
        var isMaxYear = this.year === this.maxYear;
        var range = computeMonthRange(this.range, this.year);
        Array.from(this.grid.children).forEach(function (el, index) {
          var classList = el.classList;
          var date = dateValue(_this4.year, index, 1);
          el.className = "datepicker-cell hover:bg-gray-100 block flex-1 leading-9 border-0 rounded-lg cursor-pointer text-center text-gray-900 font-semibold text-sm ".concat(_this4.cellClass);

          if (_this4.isMinView) {
            el.dataset.date = date;
          } // reset text on every render to clear the custom content set
          // by beforeShow hook at previous render


          el.textContent = _this4.monthNames[index];

          if (yrOutOfRange || isMinYear && index < _this4.minMonth || isMaxYear && index > _this4.maxMonth) {
            classList.add('disabled');
          }

          if (range) {
            var _range2 = _slicedToArray(range, 2),
                rangeStart = _range2[0],
                rangeEnd = _range2[1];

            if (index > rangeStart && index < rangeEnd) {
              classList.add('range');
            }

            if (index === rangeStart) {
              classList.add('range-start');
            }

            if (index === rangeEnd) {
              classList.add('range-end');
            }
          }

          if (selected.includes(index)) {
            classList.add('selected', 'bg-blue-500', 'text-white');
            classList.remove('text-gray-900', 'hover:bg-gray-100');
          }

          if (index === _this4.focused) {
            classList.add('focused', 'bg-gray-100');
          }

          if (_this4.beforeShow) {
            _this4.performBeforeHook(el, index, date);
          }
        });
      } // Update the view UI by applying the changes of selected and focused items

    }, {
      key: "refresh",
      value: function refresh() {
        var _this5 = this;

        var selected = this.selected[this.year] || [];

        var _ref4 = computeMonthRange(this.range, this.year) || [],
            _ref5 = _slicedToArray(_ref4, 2),
            rangeStart = _ref5[0],
            rangeEnd = _ref5[1];

        this.grid.querySelectorAll('.range, .range-start, .range-end, .selected, .focused').forEach(function (el) {
          el.classList.remove('range', 'range-start', 'range-end', 'selected', 'bg-blue-500', 'text-white', 'focused', 'bg-gray-100');
          el.classList.add('text-gray-900', 'hover:bg-gray-100');
        });
        Array.from(this.grid.children).forEach(function (el, index) {
          var classList = el.classList;

          if (index > rangeStart && index < rangeEnd) {
            classList.add('range');
          }

          if (index === rangeStart) {
            classList.add('range-start');
          }

          if (index === rangeEnd) {
            classList.add('range-end');
          }

          if (selected.includes(index)) {
            classList.add('selected', 'bg-blue-500', 'text-white');
            classList.remove('text-gray-900', 'hover:bg-gray-100');
          }

          if (index === _this5.focused) {
            classList.add('focused', 'bg-gray-100');
          }
        });
      } // Update the view UI by applying the change of focused item

    }, {
      key: "refreshFocus",
      value: function refreshFocus() {
        this.grid.querySelectorAll('.focused').forEach(function (el) {
          el.classList.remove('focused', 'bg-gray-100');
        });
        this.grid.children[this.focused].classList.add('focused', 'bg-gray-100');
      }
    }]);

    return MonthsView;
  }(View);

  function toTitleCase(word) {
    return _toConsumableArray(word).reduce(function (str, ch, ix) {
      return str += ix ? ch : ch.toUpperCase();
    }, '');
  } // Class representing the years and decades view elements


  var YearsView = /*#__PURE__*/function (_View3) {
    _inherits(YearsView, _View3);

    var _super3 = _createSuper(YearsView);

    function YearsView(picker, config) {
      _classCallCheck(this, YearsView);

      return _super3.call(this, picker, config);
    }

    _createClass(YearsView, [{
      key: "init",
      value: function init(options) {
        var onConstruction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

        if (onConstruction) {
          this.navStep = this.step * 10;
          this.beforeShowOption = "beforeShow".concat(toTitleCase(this.cellClass));
          this.grid = this.element;
          this.element.classList.add(this.name, 'datepicker-grid', 'w-64', 'grid', 'grid-cols-4');
          this.grid.appendChild(parseHTML(createTagRepeat('span', 12)));
        }

        _get(_getPrototypeOf(YearsView.prototype), "init", this).call(this, options);
      }
    }, {
      key: "setOptions",
      value: function setOptions(options) {
        if (hasProperty(options, 'minDate')) {
          if (options.minDate === undefined) {
            this.minYear = this.minDate = undefined;
          } else {
            this.minYear = startOfYearPeriod(options.minDate, this.step);
            this.minDate = dateValue(this.minYear, 0, 1);
          }
        }

        if (hasProperty(options, 'maxDate')) {
          if (options.maxDate === undefined) {
            this.maxYear = this.maxDate = undefined;
          } else {
            this.maxYear = startOfYearPeriod(options.maxDate, this.step);
            this.maxDate = dateValue(this.maxYear, 11, 31);
          }
        }

        if (options[this.beforeShowOption] !== undefined) {
          var beforeShow = options[this.beforeShowOption];
          this.beforeShow = typeof beforeShow === 'function' ? beforeShow : undefined;
        }
      } // Update view's settings to reflect the viewDate set on the picker

    }, {
      key: "updateFocus",
      value: function updateFocus() {
        var viewDate = new Date(this.picker.viewDate);
        var first = startOfYearPeriod(viewDate, this.navStep);
        var last = first + 9 * this.step;
        this.first = first;
        this.last = last;
        this.start = first - this.step;
        this.focused = startOfYearPeriod(viewDate, this.step);
      } // Update view's settings to reflect the selected dates

    }, {
      key: "updateSelection",
      value: function updateSelection() {
        var _this6 = this;

        var _this$picker$datepick3 = this.picker.datepicker,
            dates = _this$picker$datepick3.dates,
            rangepicker = _this$picker$datepick3.rangepicker;
        this.selected = dates.reduce(function (years, timeValue) {
          return pushUnique(years, startOfYearPeriod(timeValue, _this6.step));
        }, []);

        if (rangepicker && rangepicker.dates) {
          this.range = rangepicker.dates.map(function (timeValue) {
            if (timeValue !== undefined) {
              return startOfYearPeriod(timeValue, _this6.step);
            }
          });
        }
      } // Update the entire view UI

    }, {
      key: "render",
      value: function render() {
        var _this7 = this;

        // refresh disabled years on every render in order to clear the ones added
        // by beforeShow hook at previous render
        this.disabled = [];
        this.picker.setViewSwitchLabel("".concat(this.first, "-").concat(this.last));
        this.picker.setPrevBtnDisabled(this.first <= this.minYear);
        this.picker.setNextBtnDisabled(this.last >= this.maxYear);
        Array.from(this.grid.children).forEach(function (el, index) {
          var classList = el.classList;
          var current = _this7.start + index * _this7.step;
          var date = dateValue(current, 0, 1);
          el.className = "datepicker-cell hover:bg-gray-100 block flex-1 leading-9 border-0 rounded-lg cursor-pointer text-center text-gray-900 font-semibold text-sm ".concat(_this7.cellClass);

          if (_this7.isMinView) {
            el.dataset.date = date;
          }

          el.textContent = el.dataset.year = current;

          if (index === 0) {
            classList.add('prev');
          } else if (index === 11) {
            classList.add('next');
          }

          if (current < _this7.minYear || current > _this7.maxYear) {
            classList.add('disabled');
          }

          if (_this7.range) {
            var _this7$range = _slicedToArray(_this7.range, 2),
                rangeStart = _this7$range[0],
                rangeEnd = _this7$range[1];

            if (current > rangeStart && current < rangeEnd) {
              classList.add('range');
            }

            if (current === rangeStart) {
              classList.add('range-start');
            }

            if (current === rangeEnd) {
              classList.add('range-end');
            }
          }

          if (_this7.selected.includes(current)) {
            classList.add('selected', 'bg-blue-500', 'text-white');
            classList.remove('text-gray-900', 'hover:bg-gray-100');
          }

          if (current === _this7.focused) {
            classList.add('focused', 'bg-gray-100');
          }

          if (_this7.beforeShow) {
            _this7.performBeforeHook(el, current, date);
          }
        });
      } // Update the view UI by applying the changes of selected and focused items

    }, {
      key: "refresh",
      value: function refresh() {
        var _this8 = this;

        var _ref6 = this.range || [],
            _ref7 = _slicedToArray(_ref6, 2),
            rangeStart = _ref7[0],
            rangeEnd = _ref7[1];

        this.grid.querySelectorAll('.range, .range-start, .range-end, .selected, .focused').forEach(function (el) {
          el.classList.remove('range', 'range-start', 'range-end', 'selected', 'bg-blue-500', 'text-white', 'focused', 'bg-gray-100');
        });
        Array.from(this.grid.children).forEach(function (el) {
          var current = Number(el.textContent);
          var classList = el.classList;

          if (current > rangeStart && current < rangeEnd) {
            classList.add('range');
          }

          if (current === rangeStart) {
            classList.add('range-start');
          }

          if (current === rangeEnd) {
            classList.add('range-end');
          }

          if (_this8.selected.includes(current)) {
            classList.add('selected', 'bg-blue-500', 'text-white');
            classList.remove('text-gray-900', 'hover:bg-gray-100');
          }

          if (current === _this8.focused) {
            classList.add('focused', 'bg-gray-100');
          }
        });
      } // Update the view UI by applying the change of focused item

    }, {
      key: "refreshFocus",
      value: function refreshFocus() {
        var index = Math.round((this.focused - this.start) / this.step);
        this.grid.querySelectorAll('.focused').forEach(function (el) {
          el.classList.remove('focused', 'bg-gray-100');
        });
        this.grid.children[index].classList.add('focused', 'bg-gray-100');
      }
    }]);

    return YearsView;
  }(View);

  function triggerDatepickerEvent(datepicker, type) {
    var detail = {
      date: datepicker.getDate(),
      viewDate: new Date(datepicker.picker.viewDate),
      viewId: datepicker.picker.currentView.id,
      datepicker: datepicker
    };
    datepicker.element.dispatchEvent(new CustomEvent(type, {
      detail: detail
    }));
  } // direction: -1 (to previous), 1 (to next)


  function goToPrevOrNext(datepicker, direction) {
    var _datepicker$config = datepicker.config,
        minDate = _datepicker$config.minDate,
        maxDate = _datepicker$config.maxDate;
    var _datepicker$picker = datepicker.picker,
        currentView = _datepicker$picker.currentView,
        viewDate = _datepicker$picker.viewDate;
    var newViewDate;

    switch (currentView.id) {
      case 0:
        newViewDate = addMonths(viewDate, direction);
        break;

      case 1:
        newViewDate = addYears(viewDate, direction);
        break;

      default:
        newViewDate = addYears(viewDate, direction * currentView.navStep);
    }

    newViewDate = limitToRange(newViewDate, minDate, maxDate);
    datepicker.picker.changeFocus(newViewDate).render();
  }

  function switchView(datepicker) {
    var viewId = datepicker.picker.currentView.id;

    if (viewId === datepicker.config.maxView) {
      return;
    }

    datepicker.picker.changeView(viewId + 1).render();
  }

  function unfocus(datepicker) {
    if (datepicker.config.updateOnBlur) {
      datepicker.update({
        autohide: true
      });
    } else {
      datepicker.refresh('input');
      datepicker.hide();
    }
  }

  function goToSelectedMonthOrYear(datepicker, selection) {
    var picker = datepicker.picker;
    var viewDate = new Date(picker.viewDate);
    var viewId = picker.currentView.id;
    var newDate = viewId === 1 ? addMonths(viewDate, selection - viewDate.getMonth()) : addYears(viewDate, selection - viewDate.getFullYear());
    picker.changeFocus(newDate).changeView(viewId - 1).render();
  }

  function onClickTodayBtn(datepicker) {
    var picker = datepicker.picker;
    var currentDate = today();

    if (datepicker.config.todayBtnMode === 1) {
      if (datepicker.config.autohide) {
        datepicker.setDate(currentDate);
        return;
      }

      datepicker.setDate(currentDate, {
        render: false
      });
      picker.update();
    }

    if (picker.viewDate !== currentDate) {
      picker.changeFocus(currentDate);
    }

    picker.changeView(0).render();
  }

  function onClickClearBtn(datepicker) {
    datepicker.setDate({
      clear: true
    });
  }

  function onClickViewSwitch(datepicker) {
    switchView(datepicker);
  }

  function onClickPrevBtn(datepicker) {
    goToPrevOrNext(datepicker, -1);
  }

  function onClickNextBtn(datepicker) {
    goToPrevOrNext(datepicker, 1);
  } // For the picker's main block to delegete the events from `datepicker-cell`s


  function onClickView(datepicker, ev) {
    var target = findElementInEventPath(ev, '.datepicker-cell');

    if (!target || target.classList.contains('disabled')) {
      return;
    }

    var _datepicker$picker$cu = datepicker.picker.currentView,
        id = _datepicker$picker$cu.id,
        isMinView = _datepicker$picker$cu.isMinView;

    if (isMinView) {
      datepicker.setDate(Number(target.dataset.date));
    } else if (id === 1) {
      goToSelectedMonthOrYear(datepicker, Number(target.dataset.month));
    } else {
      goToSelectedMonthOrYear(datepicker, Number(target.dataset.year));
    }
  }

  function onClickPicker(datepicker) {
    if (!datepicker.inline && !datepicker.config.disableTouchKeyboard) {
      datepicker.inputField.focus();
    }
  }

  function processPickerOptions(picker, options) {
    if (options.title !== undefined) {
      if (options.title) {
        picker.controls.title.textContent = options.title;
        showElement(picker.controls.title);
      } else {
        picker.controls.title.textContent = '';
        hideElement(picker.controls.title);
      }
    }

    if (options.prevArrow) {
      var prevBtn = picker.controls.prevBtn;
      emptyChildNodes(prevBtn);
      options.prevArrow.forEach(function (node) {
        prevBtn.appendChild(node.cloneNode(true));
      });
    }

    if (options.nextArrow) {
      var nextBtn = picker.controls.nextBtn;
      emptyChildNodes(nextBtn);
      options.nextArrow.forEach(function (node) {
        nextBtn.appendChild(node.cloneNode(true));
      });
    }

    if (options.locale) {
      picker.controls.todayBtn.textContent = options.locale.today;
      picker.controls.clearBtn.textContent = options.locale.clear;
    }

    if (options.todayBtn !== undefined) {
      if (options.todayBtn) {
        showElement(picker.controls.todayBtn);
      } else {
        hideElement(picker.controls.todayBtn);
      }
    }

    if (hasProperty(options, 'minDate') || hasProperty(options, 'maxDate')) {
      var _picker$datepicker$co = picker.datepicker.config,
          minDate = _picker$datepicker$co.minDate,
          maxDate = _picker$datepicker$co.maxDate;
      picker.controls.todayBtn.disabled = !isInRange(today(), minDate, maxDate);
    }

    if (options.clearBtn !== undefined) {
      if (options.clearBtn) {
        showElement(picker.controls.clearBtn);
      } else {
        hideElement(picker.controls.clearBtn);
      }
    }
  } // Compute view date to reset, which will be...
  // - the last item of the selected dates or defaultViewDate if no selection
  // - limitted to minDate or maxDate if it exceeds the range


  function computeResetViewDate(datepicker) {
    var dates = datepicker.dates,
        config = datepicker.config;
    var viewDate = dates.length > 0 ? lastItemOf(dates) : config.defaultViewDate;
    return limitToRange(viewDate, config.minDate, config.maxDate);
  } // Change current view's view date


  function setViewDate(picker, newDate) {
    var oldViewDate = new Date(picker.viewDate);
    var newViewDate = new Date(newDate);
    var _picker$currentView = picker.currentView,
        id = _picker$currentView.id,
        year = _picker$currentView.year,
        first = _picker$currentView.first,
        last = _picker$currentView.last;
    var viewYear = newViewDate.getFullYear();
    picker.viewDate = newDate;

    if (viewYear !== oldViewDate.getFullYear()) {
      triggerDatepickerEvent(picker.datepicker, 'changeYear');
    }

    if (newViewDate.getMonth() !== oldViewDate.getMonth()) {
      triggerDatepickerEvent(picker.datepicker, 'changeMonth');
    } // return whether the new date is in different period on time from the one
    // displayed in the current view
    // when true, the view needs to be re-rendered on the next UI refresh.


    switch (id) {
      case 0:
        return newDate < first || newDate > last;

      case 1:
        return viewYear !== year;

      default:
        return viewYear < first || viewYear > last;
    }
  }

  function getTextDirection(el) {
    return window.getComputedStyle(el).direction;
  } // Class representing the picker UI


  var Picker = /*#__PURE__*/function () {
    function Picker(datepicker) {
      _classCallCheck(this, Picker);

      this.datepicker = datepicker;
      var template = pickerTemplate.replace(/%buttonClass%/g, datepicker.config.buttonClass);
      var element = this.element = parseHTML(template).firstChild;

      var _element$firstChild$c = _slicedToArray(element.firstChild.children, 3),
          header = _element$firstChild$c[0],
          main = _element$firstChild$c[1],
          footer = _element$firstChild$c[2];

      var title = header.firstElementChild;

      var _header$lastElementCh = _slicedToArray(header.lastElementChild.children, 3),
          prevBtn = _header$lastElementCh[0],
          viewSwitch = _header$lastElementCh[1],
          nextBtn = _header$lastElementCh[2];

      var _footer$firstChild$ch = _slicedToArray(footer.firstChild.children, 2),
          todayBtn = _footer$firstChild$ch[0],
          clearBtn = _footer$firstChild$ch[1];

      var controls = {
        title: title,
        prevBtn: prevBtn,
        viewSwitch: viewSwitch,
        nextBtn: nextBtn,
        todayBtn: todayBtn,
        clearBtn: clearBtn
      };
      this.main = main;
      this.controls = controls;
      var elementClass = datepicker.inline ? 'inline' : 'dropdown';
      element.classList.add("datepicker-".concat(elementClass));
      elementClass === 'dropdown' ? element.classList.add('dropdown', 'absolute', 'top-0', 'left-0', 'z-20', 'pt-2') : null;
      processPickerOptions(this, datepicker.config);
      this.viewDate = computeResetViewDate(datepicker); // set up event listeners

      registerListeners(datepicker, [[element, 'click', onClickPicker.bind(null, datepicker), {
        capture: true
      }], [main, 'click', onClickView.bind(null, datepicker)], [controls.viewSwitch, 'click', onClickViewSwitch.bind(null, datepicker)], [controls.prevBtn, 'click', onClickPrevBtn.bind(null, datepicker)], [controls.nextBtn, 'click', onClickNextBtn.bind(null, datepicker)], [controls.todayBtn, 'click', onClickTodayBtn.bind(null, datepicker)], [controls.clearBtn, 'click', onClickClearBtn.bind(null, datepicker)]]); // set up views

      this.views = [new DaysView(this), new MonthsView(this), new YearsView(this, {
        id: 2,
        name: 'years',
        cellClass: 'year',
        step: 1
      }), new YearsView(this, {
        id: 3,
        name: 'decades',
        cellClass: 'decade',
        step: 10
      })];
      this.currentView = this.views[datepicker.config.startView];
      this.currentView.render();
      this.main.appendChild(this.currentView.element);
      datepicker.config.container.appendChild(this.element);
    }

    _createClass(Picker, [{
      key: "setOptions",
      value: function setOptions(options) {
        processPickerOptions(this, options);
        this.views.forEach(function (view) {
          view.init(options, false);
        });
        this.currentView.render();
      }
    }, {
      key: "detach",
      value: function detach() {
        this.datepicker.config.container.removeChild(this.element);
      }
    }, {
      key: "show",
      value: function show() {
        if (this.active) {
          return;
        }

        this.element.classList.add('active', 'block');
        this.element.classList.remove('hidden');
        this.active = true;
        var datepicker = this.datepicker;

        if (!datepicker.inline) {
          // ensure picker's direction matches input's
          var inputDirection = getTextDirection(datepicker.inputField);

          if (inputDirection !== getTextDirection(datepicker.config.container)) {
            this.element.dir = inputDirection;
          } else if (this.element.dir) {
            this.element.removeAttribute('dir');
          }

          this.place();

          if (datepicker.config.disableTouchKeyboard) {
            datepicker.inputField.blur();
          }
        }

        triggerDatepickerEvent(datepicker, 'show');
      }
    }, {
      key: "hide",
      value: function hide() {
        if (!this.active) {
          return;
        }

        this.datepicker.exitEditMode();
        this.element.classList.remove('active', 'block');
        this.element.classList.add('active', 'block', 'hidden');
        this.active = false;
        triggerDatepickerEvent(this.datepicker, 'hide');
      }
    }, {
      key: "place",
      value: function place() {
        var _this$element = this.element,
            classList = _this$element.classList,
            style = _this$element.style;
        var _this$datepicker = this.datepicker,
            config = _this$datepicker.config,
            inputField = _this$datepicker.inputField;
        var container = config.container;

        var _this$element$getBoun = this.element.getBoundingClientRect(),
            calendarWidth = _this$element$getBoun.width,
            calendarHeight = _this$element$getBoun.height;

        var _container$getBoundin = container.getBoundingClientRect(),
            containerLeft = _container$getBoundin.left,
            containerTop = _container$getBoundin.top,
            containerWidth = _container$getBoundin.width;

        var _inputField$getBoundi = inputField.getBoundingClientRect(),
            inputLeft = _inputField$getBoundi.left,
            inputTop = _inputField$getBoundi.top,
            inputWidth = _inputField$getBoundi.width,
            inputHeight = _inputField$getBoundi.height;

        var _config$orientation = config.orientation,
            orientX = _config$orientation.x,
            orientY = _config$orientation.y;
        var scrollTop;
        var left;
        var top;

        if (container === document.body) {
          scrollTop = window.scrollY;
          left = inputLeft + window.scrollX;
          top = inputTop + scrollTop;
        } else {
          scrollTop = container.scrollTop;
          left = inputLeft - containerLeft;
          top = inputTop - containerTop + scrollTop;
        }

        if (orientX === 'auto') {
          if (left < 0) {
            // align to the left and move into visible area if input's left edge < window's
            orientX = 'left';
            left = 10;
          } else if (left + calendarWidth > containerWidth) {
            // align to the right if canlendar's right edge > container's
            orientX = 'right';
          } else {
            orientX = getTextDirection(inputField) === 'rtl' ? 'right' : 'left';
          }
        }

        if (orientX === 'right') {
          left -= calendarWidth - inputWidth;
        }

        if (orientY === 'auto') {
          orientY = top - calendarHeight < scrollTop ? 'bottom' : 'top';
        }

        if (orientY === 'top') {
          top -= calendarHeight;
        } else {
          top += inputHeight;
        }

        classList.remove('datepicker-orient-top', 'datepicker-orient-bottom', 'datepicker-orient-right', 'datepicker-orient-left');
        classList.add("datepicker-orient-".concat(orientY), "datepicker-orient-".concat(orientX));
        style.top = top ? "".concat(top, "px") : top;
        style.left = left ? "".concat(left, "px") : left;
      }
    }, {
      key: "setViewSwitchLabel",
      value: function setViewSwitchLabel(labelText) {
        this.controls.viewSwitch.textContent = labelText;
      }
    }, {
      key: "setPrevBtnDisabled",
      value: function setPrevBtnDisabled(disabled) {
        this.controls.prevBtn.disabled = disabled;
      }
    }, {
      key: "setNextBtnDisabled",
      value: function setNextBtnDisabled(disabled) {
        this.controls.nextBtn.disabled = disabled;
      }
    }, {
      key: "changeView",
      value: function changeView(viewId) {
        var oldView = this.currentView;
        var newView = this.views[viewId];

        if (newView.id !== oldView.id) {
          this.currentView = newView;
          this._renderMethod = 'render';
          triggerDatepickerEvent(this.datepicker, 'changeView');
          this.main.replaceChild(newView.element, oldView.element);
        }

        return this;
      } // Change the focused date (view date)

    }, {
      key: "changeFocus",
      value: function changeFocus(newViewDate) {
        this._renderMethod = setViewDate(this, newViewDate) ? 'render' : 'refreshFocus';
        this.views.forEach(function (view) {
          view.updateFocus();
        });
        return this;
      } // Apply the change of the selected dates

    }, {
      key: "update",
      value: function update() {
        var newViewDate = computeResetViewDate(this.datepicker);
        this._renderMethod = setViewDate(this, newViewDate) ? 'render' : 'refresh';
        this.views.forEach(function (view) {
          view.updateFocus();
          view.updateSelection();
        });
        return this;
      } // Refresh the picker UI

    }, {
      key: "render",
      value: function render() {
        var quickRender = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
        var renderMethod = quickRender && this._renderMethod || 'render';
        delete this._renderMethod;
        this.currentView[renderMethod]();
      }
    }]);

    return Picker;
  }(); // Find the closest date that doesn't meet the condition for unavailable date
  // Returns undefined if no available date is found
  // addFn: function to calculate the next date
  //   - args: time value, amount
  // increase: amount to pass to addFn
  // testFn: function to test the unavailablity of the date
  //   - args: time value; retun: true if unavailable


  function findNextAvailableOne(date, addFn, increase, testFn, min, max) {
    if (!isInRange(date, min, max)) {
      return;
    }

    if (testFn(date)) {
      var newDate = addFn(date, increase);
      return findNextAvailableOne(newDate, addFn, increase, testFn, min, max);
    }

    return date;
  } // direction: -1 (left/up), 1 (right/down)
  // vertical: true for up/down, false for left/right


  function moveByArrowKey(datepicker, ev, direction, vertical) {
    var picker = datepicker.picker;
    var currentView = picker.currentView;
    var step = currentView.step || 1;
    var viewDate = picker.viewDate;
    var addFn;
    var testFn;

    switch (currentView.id) {
      case 0:
        if (vertical) {
          viewDate = addDays(viewDate, direction * 7);
        } else if (ev.ctrlKey || ev.metaKey) {
          viewDate = addYears(viewDate, direction);
        } else {
          viewDate = addDays(viewDate, direction);
        }

        addFn = addDays;

        testFn = function testFn(date) {
          return currentView.disabled.includes(date);
        };

        break;

      case 1:
        viewDate = addMonths(viewDate, vertical ? direction * 4 : direction);
        addFn = addMonths;

        testFn = function testFn(date) {
          var dt = new Date(date);
          var year = currentView.year,
              disabled = currentView.disabled;
          return dt.getFullYear() === year && disabled.includes(dt.getMonth());
        };

        break;

      default:
        viewDate = addYears(viewDate, direction * (vertical ? 4 : 1) * step);
        addFn = addYears;

        testFn = function testFn(date) {
          return currentView.disabled.includes(startOfYearPeriod(date, step));
        };

    }

    viewDate = findNextAvailableOne(viewDate, addFn, direction < 0 ? -step : step, testFn, currentView.minDate, currentView.maxDate);

    if (viewDate !== undefined) {
      picker.changeFocus(viewDate).render();
    }
  }

  function onKeydown(datepicker, ev) {
    if (ev.key === 'Tab') {
      unfocus(datepicker);
      return;
    }

    var picker = datepicker.picker;
    var _picker$currentView2 = picker.currentView,
        id = _picker$currentView2.id,
        isMinView = _picker$currentView2.isMinView;

    if (!picker.active) {
      switch (ev.key) {
        case 'ArrowDown':
        case 'Escape':
          picker.show();
          break;

        case 'Enter':
          datepicker.update();
          break;

        default:
          return;
      }
    } else if (datepicker.editMode) {
      switch (ev.key) {
        case 'Escape':
          picker.hide();
          break;

        case 'Enter':
          datepicker.exitEditMode({
            update: true,
            autohide: datepicker.config.autohide
          });
          break;

        default:
          return;
      }
    } else {
      switch (ev.key) {
        case 'Escape':
          picker.hide();
          break;

        case 'ArrowLeft':
          if (ev.ctrlKey || ev.metaKey) {
            goToPrevOrNext(datepicker, -1);
          } else if (ev.shiftKey) {
            datepicker.enterEditMode();
            return;
          } else {
            moveByArrowKey(datepicker, ev, -1, false);
          }

          break;

        case 'ArrowRight':
          if (ev.ctrlKey || ev.metaKey) {
            goToPrevOrNext(datepicker, 1);
          } else if (ev.shiftKey) {
            datepicker.enterEditMode();
            return;
          } else {
            moveByArrowKey(datepicker, ev, 1, false);
          }

          break;

        case 'ArrowUp':
          if (ev.ctrlKey || ev.metaKey) {
            switchView(datepicker);
          } else if (ev.shiftKey) {
            datepicker.enterEditMode();
            return;
          } else {
            moveByArrowKey(datepicker, ev, -1, true);
          }

          break;

        case 'ArrowDown':
          if (ev.shiftKey && !ev.ctrlKey && !ev.metaKey) {
            datepicker.enterEditMode();
            return;
          }

          moveByArrowKey(datepicker, ev, 1, true);
          break;

        case 'Enter':
          if (isMinView) {
            datepicker.setDate(picker.viewDate);
          } else {
            picker.changeView(id - 1).render();
          }

          break;

        case 'Backspace':
        case 'Delete':
          datepicker.enterEditMode();
          return;

        default:
          if (ev.key.length === 1 && !ev.ctrlKey && !ev.metaKey) {
            datepicker.enterEditMode();
          }

          return;
      }
    }

    ev.preventDefault();
    ev.stopPropagation();
  }

  function onFocus(datepicker) {
    if (datepicker.config.showOnFocus && !datepicker._showing) {
      datepicker.show();
    }
  } // for the prevention for entering edit mode while getting focus on click


  function onMousedown(datepicker, ev) {
    var el = ev.target;

    if (datepicker.picker.active || datepicker.config.showOnClick) {
      el._active = el === document.activeElement;
      el._clicking = setTimeout(function () {
        delete el._active;
        delete el._clicking;
      }, 2000);
    }
  }

  function onClickInput(datepicker, ev) {
    var el = ev.target;

    if (!el._clicking) {
      return;
    }

    clearTimeout(el._clicking);
    delete el._clicking;

    if (el._active) {
      datepicker.enterEditMode();
    }

    delete el._active;

    if (datepicker.config.showOnClick) {
      datepicker.show();
    }
  }

  function onPaste(datepicker, ev) {
    if (ev.clipboardData.types.includes('text/plain')) {
      datepicker.enterEditMode();
    }
  } // for the `document` to delegate the events from outside the picker/input field


  function onClickOutside(datepicker, ev) {
    var element = datepicker.element;

    if (element !== document.activeElement) {
      return;
    }

    var pickerElem = datepicker.picker.element;

    if (findElementInEventPath(ev, function (el) {
      return el === element || el === pickerElem;
    })) {
      return;
    }

    unfocus(datepicker);
  }

  function stringifyDates(dates, config) {
    return dates.map(function (dt) {
      return _formatDate(dt, config.format, config.locale);
    }).join(config.dateDelimiter);
  } // parse input dates and create an array of time values for selection
  // returns undefined if there are no valid dates in inputDates
  // when origDates (current selection) is passed, the function works to mix
  // the input dates into the current selection


  function processInputDates(datepicker, inputDates) {
    var clear = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var config = datepicker.config,
        origDates = datepicker.dates,
        rangepicker = datepicker.rangepicker;

    if (inputDates.length === 0) {
      // empty input is considered valid unless origiDates is passed
      return clear ? [] : undefined;
    }

    var rangeEnd = rangepicker && datepicker === rangepicker.datepickers[1];
    var newDates = inputDates.reduce(function (dates, dt) {
      var date = _parseDate(dt, config.format, config.locale);

      if (date === undefined) {
        return dates;
      }

      if (config.pickLevel > 0) {
        // adjust to 1st of the month/Jan 1st of the year
        // or to the last day of the monh/Dec 31st of the year if the datepicker
        // is the range-end picker of a rangepicker
        var _dt = new Date(date);

        if (config.pickLevel === 1) {
          date = rangeEnd ? _dt.setMonth(_dt.getMonth() + 1, 0) : _dt.setDate(1);
        } else {
          date = rangeEnd ? _dt.setFullYear(_dt.getFullYear() + 1, 0, 0) : _dt.setMonth(0, 1);
        }
      }

      if (isInRange(date, config.minDate, config.maxDate) && !dates.includes(date) && !config.datesDisabled.includes(date) && !config.daysOfWeekDisabled.includes(new Date(date).getDay())) {
        dates.push(date);
      }

      return dates;
    }, []);

    if (newDates.length === 0) {
      return;
    }

    if (config.multidate && !clear) {
      // get the synmetric difference between origDates and newDates
      newDates = newDates.reduce(function (dates, date) {
        if (!origDates.includes(date)) {
          dates.push(date);
        }

        return dates;
      }, origDates.filter(function (date) {
        return !newDates.includes(date);
      }));
    } // do length check always because user can input multiple dates regardless of the mode


    return config.maxNumberOfDates && newDates.length > config.maxNumberOfDates ? newDates.slice(config.maxNumberOfDates * -1) : newDates;
  } // refresh the UI elements
  // modes: 1: input only, 2, picker only, 3 both


  function refreshUI(datepicker) {
    var mode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3;
    var quickRender = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    var config = datepicker.config,
        picker = datepicker.picker,
        inputField = datepicker.inputField;

    if (mode & 2) {
      var newView = picker.active ? config.pickLevel : config.startView;
      picker.update().changeView(newView).render(quickRender);
    }

    if (mode & 1 && inputField) {
      inputField.value = stringifyDates(datepicker.dates, config);
    }
  }

  function _setDate(datepicker, inputDates, options) {
    var clear = options.clear,
        render = options.render,
        autohide = options.autohide;

    if (render === undefined) {
      render = true;
    }

    if (!render) {
      autohide = false;
    } else if (autohide === undefined) {
      autohide = datepicker.config.autohide;
    }

    var newDates = processInputDates(datepicker, inputDates, clear);

    if (!newDates) {
      return;
    }

    if (newDates.toString() !== datepicker.dates.toString()) {
      datepicker.dates = newDates;
      refreshUI(datepicker, render ? 3 : 1);
      triggerDatepickerEvent(datepicker, 'changeDate');
    } else {
      refreshUI(datepicker, 1);
    }

    if (autohide) {
      datepicker.hide();
    }
  }
  /**
   * Class representing a date picker
   */


  var Datepicker = /*#__PURE__*/function () {
    /**
     * Create a date picker
     * @param  {Element} element - element to bind a date picker
     * @param  {Object} [options] - config options
     * @param  {DateRangePicker} [rangepicker] - DateRangePicker instance the
     * date picker belongs to. Use this only when creating date picker as a part
     * of date range picker
     */
    function Datepicker(element) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var rangepicker = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;

      _classCallCheck(this, Datepicker);

      element.datepicker = this;
      this.element = element; // set up config

      var config = this.config = Object.assign({
        buttonClass: options.buttonClass && String(options.buttonClass) || 'button',
        container: document.body,
        defaultViewDate: today(),
        maxDate: undefined,
        minDate: undefined
      }, processOptions(defaultOptions, this));
      this._options = options;
      Object.assign(config, processOptions(options, this)); // configure by type

      var inline = this.inline = element.tagName !== 'INPUT';
      var inputField;
      var initialDates;

      if (inline) {
        config.container = element;
        initialDates = stringToArray(element.dataset.date, config.dateDelimiter);
        delete element.dataset.date;
      } else {
        var container = options.container ? document.querySelector(options.container) : null;

        if (container) {
          config.container = container;
        }

        inputField = this.inputField = element;
        inputField.classList.add('datepicker-input');
        initialDates = stringToArray(inputField.value, config.dateDelimiter);
      }

      if (rangepicker) {
        // check validiry
        var index = rangepicker.inputs.indexOf(inputField);
        var datepickers = rangepicker.datepickers;

        if (index < 0 || index > 1 || !Array.isArray(datepickers)) {
          throw Error('Invalid rangepicker object.');
        } // attach itaelf to the rangepicker here so that processInputDates() can
        // determine if this is the range-end picker of the rangepicker while
        // setting inital values when pickLevel > 0


        datepickers[index] = this; // add getter for rangepicker

        Object.defineProperty(this, 'rangepicker', {
          get: function get() {
            return rangepicker;
          }
        });
      } // set initial dates


      this.dates = []; // process initial value

      var inputDateValues = processInputDates(this, initialDates);

      if (inputDateValues && inputDateValues.length > 0) {
        this.dates = inputDateValues;
      }

      if (inputField) {
        inputField.value = stringifyDates(this.dates, config);
      }

      var picker = this.picker = new Picker(this);

      if (inline) {
        this.show();
      } else {
        // set up event listeners in other modes
        var onMousedownDocument = onClickOutside.bind(null, this);
        var listeners = [[inputField, 'keydown', onKeydown.bind(null, this)], [inputField, 'focus', onFocus.bind(null, this)], [inputField, 'mousedown', onMousedown.bind(null, this)], [inputField, 'click', onClickInput.bind(null, this)], [inputField, 'paste', onPaste.bind(null, this)], [document, 'mousedown', onMousedownDocument], [document, 'touchstart', onMousedownDocument], [window, 'resize', picker.place.bind(picker)]];
        registerListeners(this, listeners);
      }
    }
    /**
     * Format Date object or time value in given format and language
     * @param  {Date|Number} date - date or time value to format
     * @param  {String|Object} format - format string or object that contains
     * toDisplay() custom formatter, whose signature is
     * - args:
     *   - date: {Date} - Date instance of the date passed to the method
     *   - format: {Object} - the format object passed to the method
     *   - locale: {Object} - locale for the language specified by `lang`
     * - return:
     *     {String} formatted date
     * @param  {String} [lang=en] - language code for the locale to use
     * @return {String} formatted date
     */


    _createClass(Datepicker, [{
      key: "active",
      get:
      /**
       * @type {Boolean} - Whether the picker element is shown. `true` whne shown
       */
      function get() {
        return !!(this.picker && this.picker.active);
      }
      /**
       * @type {HTMLDivElement} - DOM object of picker element
       */

    }, {
      key: "pickerElement",
      get: function get() {
        return this.picker ? this.picker.element : undefined;
      }
      /**
       * Set new values to the config options
       * @param {Object} options - config options to update
       */

    }, {
      key: "setOptions",
      value: function setOptions(options) {
        var picker = this.picker;
        var newOptions = processOptions(options, this);
        Object.assign(this._options, options);
        Object.assign(this.config, newOptions);
        picker.setOptions(newOptions);
        refreshUI(this, 3);
      }
      /**
       * Show the picker element
       */

    }, {
      key: "show",
      value: function show() {
        if (this.inputField) {
          if (this.inputField.disabled) {
            return;
          }

          if (this.inputField !== document.activeElement) {
            this._showing = true;
            this.inputField.focus();
            delete this._showing;
          }
        }

        this.picker.show();
      }
      /**
       * Hide the picker element
       * Not available on inline picker
       */

    }, {
      key: "hide",
      value: function hide() {
        if (this.inline) {
          return;
        }

        this.picker.hide();
        this.picker.update().changeView(this.config.startView).render();
      }
      /**
       * Destroy the Datepicker instance
       * @return {Detepicker} - the instance destroyed
       */

    }, {
      key: "destroy",
      value: function destroy() {
        this.hide();
        unregisterListeners(this);
        this.picker.detach();

        if (!this.inline) {
          this.inputField.classList.remove('datepicker-input');
        }

        delete this.element.datepicker;
        return this;
      }
      /**
       * Get the selected date(s)
       *
       * The method returns a Date object of selected date by default, and returns
       * an array of selected dates in multidate mode. If format string is passed,
       * it returns date string(s) formatted in given format.
       *
       * @param  {String} [format] - Format string to stringify the date(s)
       * @return {Date|String|Date[]|String[]} - selected date(s), or if none is
       * selected, empty array in multidate mode and untitled in sigledate mode
       */

    }, {
      key: "getDate",
      value: function getDate() {
        var _this9 = this;

        var format = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
        var callback = format ? function (date) {
          return _formatDate(date, format, _this9.config.locale);
        } : function (date) {
          return new Date(date);
        };

        if (this.config.multidate) {
          return this.dates.map(callback);
        }

        if (this.dates.length > 0) {
          return callback(this.dates[0]);
        }
      }
      /**
       * Set selected date(s)
       *
       * In multidate mode, you can pass multiple dates as a series of arguments
       * or an array. (Since each date is parsed individually, the type of the
       * dates doesn't have to be the same.)
       * The given dates are used to toggle the select status of each date. The
       * number of selected dates is kept from exceeding the length set to
       * maxNumberOfDates.
       *
       * With clear: true option, the method can be used to clear the selection
       * and to replace the selection instead of toggling in multidate mode.
       * If the option is passed with no date arguments or an empty dates array,
       * it works as "clear" (clear the selection then set nothing), and if the
       * option is passed with new dates to select, it works as "replace" (clear
       * the selection then set the given dates)
       *
       * When render: false option is used, the method omits re-rendering the
       * picker element. In this case, you need to call refresh() method later in
       * order for the picker element to reflect the changes. The input field is
       * refreshed always regardless of this option.
       *
       * When invalid (unparsable, repeated, disabled or out-of-range) dates are
       * passed, the method ignores them and applies only valid ones. In the case
       * that all the given dates are invalid, which is distinguished from passing
       * no dates, the method considers it as an error and leaves the selection
       * untouched.
       *
       * @param {...(Date|Number|String)|Array} [dates] - Date strings, Date
       * objects, time values or mix of those for new selection
       * @param {Object} [options] - function options
       * - clear: {boolean} - Whether to clear the existing selection
       *     defualt: false
       * - render: {boolean} - Whether to re-render the picker element
       *     default: true
       * - autohide: {boolean} - Whether to hide the picker element after re-render
       *     Ignored when used with render: false
       *     default: config.autohide
       */

    }, {
      key: "setDate",
      value: function setDate() {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        var dates = [].concat(args);
        var opts = {};
        var lastArg = lastItemOf(args);

        if (_typeof(lastArg) === 'object' && !Array.isArray(lastArg) && !(lastArg instanceof Date) && lastArg) {
          Object.assign(opts, dates.pop());
        }

        var inputDates = Array.isArray(dates[0]) ? dates[0] : dates;

        _setDate(this, inputDates, opts);
      }
      /**
       * Update the selected date(s) with input field's value
       * Not available on inline picker
       *
       * The input field will be refreshed with properly formatted date string.
       *
       * @param  {Object} [options] - function options
       * - autohide: {boolean} - whether to hide the picker element after refresh
       *     default: false
       */

    }, {
      key: "update",
      value: function update() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

        if (this.inline) {
          return;
        }

        var opts = {
          clear: true,
          autohide: !!(options && options.autohide)
        };
        var inputDates = stringToArray(this.inputField.value, this.config.dateDelimiter);

        _setDate(this, inputDates, opts);
      }
      /**
       * Refresh the picker element and the associated input field
       * @param {String} [target] - target item when refreshing one item only
       * 'picker' or 'input'
       * @param {Boolean} [forceRender] - whether to re-render the picker element
       * regardless of its state instead of optimized refresh
       */

    }, {
      key: "refresh",
      value: function refresh() {
        var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
        var forceRender = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

        if (target && typeof target !== 'string') {
          forceRender = target;
          target = undefined;
        }

        var mode;

        if (target === 'picker') {
          mode = 2;
        } else if (target === 'input') {
          mode = 1;
        } else {
          mode = 3;
        }

        refreshUI(this, mode, !forceRender);
      }
      /**
       * Enter edit mode
       * Not available on inline picker or when the picker element is hidden
       */

    }, {
      key: "enterEditMode",
      value: function enterEditMode() {
        if (this.inline || !this.picker.active || this.editMode) {
          return;
        }

        this.editMode = true;
        this.inputField.classList.add('in-edit', 'border-blue-500');
      }
      /**
       * Exit from edit mode
       * Not available on inline picker
       * @param  {Object} [options] - function options
       * - update: {boolean} - whether to call update() after exiting
       *     If false, input field is revert to the existing selection
       *     default: false
       */

    }, {
      key: "exitEditMode",
      value: function exitEditMode() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;

        if (this.inline || !this.editMode) {
          return;
        }

        var opts = Object.assign({
          update: false
        }, options);
        delete this.editMode;
        this.inputField.classList.remove('in-edit', 'border-blue-500');

        if (opts.update) {
          this.update(opts);
        }
      }
    }], [{
      key: "formatDate",
      value: function formatDate(date, format, lang) {
        return _formatDate(date, format, lang && locales[lang] || locales.en);
      }
      /**
       * Parse date string
       * @param  {String|Date|Number} dateStr - date string, Date object or time
       * value to parse
       * @param  {String|Object} format - format string or object that contains
       * toValue() custom parser, whose signature is
       * - args:
       *   - dateStr: {String|Date|Number} - the dateStr passed to the method
       *   - format: {Object} - the format object passed to the method
       *   - locale: {Object} - locale for the language specified by `lang`
       * - return:
       *     {Date|Number} parsed date or its time value
       * @param  {String} [lang=en] - language code for the locale to use
       * @return {Number} time value of parsed date
       */

    }, {
      key: "parseDate",
      value: function parseDate(dateStr, format, lang) {
        return _parseDate(dateStr, format, lang && locales[lang] || locales.en);
      }
      /**
       * @type {Object} - Installed locales in `[languageCode]: localeObject` format
       * en`:_English (US)_ is pre-installed.
       */

    }, {
      key: "locales",
      get: function get() {
        return locales;
      }
    }]);

    return Datepicker;
  }(); // filter out the config options inapproprite to pass to Datepicker


  function filterOptions(options) {
    var newOpts = Object.assign({}, options);
    delete newOpts.inputs;
    delete newOpts.allowOneSidedRange;
    delete newOpts.maxNumberOfDates; // to ensure each datepicker handles a single date

    return newOpts;
  }

  function setupDatepicker(rangepicker, changeDateListener, el, options) {
    registerListeners(rangepicker, [[el, 'changeDate', changeDateListener]]);
    new Datepicker(el, options, rangepicker);
  }

  function onChangeDate(rangepicker, ev) {
    // to prevent both datepickers trigger the other side's update each other
    if (rangepicker._updating) {
      return;
    }

    rangepicker._updating = true;
    var target = ev.target;

    if (target.datepicker === undefined) {
      return;
    }

    var datepickers = rangepicker.datepickers;
    var setDateOptions = {
      render: false
    };
    var changedSide = rangepicker.inputs.indexOf(target);
    var otherSide = changedSide === 0 ? 1 : 0;
    var changedDate = datepickers[changedSide].dates[0];
    var otherDate = datepickers[otherSide].dates[0];

    if (changedDate !== undefined && otherDate !== undefined) {
      // if the start of the range > the end, swap them
      if (changedSide === 0 && changedDate > otherDate) {
        datepickers[0].setDate(otherDate, setDateOptions);
        datepickers[1].setDate(changedDate, setDateOptions);
      } else if (changedSide === 1 && changedDate < otherDate) {
        datepickers[0].setDate(changedDate, setDateOptions);
        datepickers[1].setDate(otherDate, setDateOptions);
      }
    } else if (!rangepicker.allowOneSidedRange) {
      // to prevent the range from becoming one-sided, copy changed side's
      // selection (no matter if it's empty) to the other side
      if (changedDate !== undefined || otherDate !== undefined) {
        setDateOptions.clear = true;
        datepickers[otherSide].setDate(datepickers[changedSide].dates, setDateOptions);
      }
    }

    datepickers[0].picker.update().render();
    datepickers[1].picker.update().render();
    delete rangepicker._updating;
  }
  /**
   * Class representing a date range picker
   */


  var DateRangePicker = /*#__PURE__*/function () {
    /**
     * Create a date range picker
     * @param  {Element} element - element to bind a date range picker
     * @param  {Object} [options] - config options
     */
    function DateRangePicker(element) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      _classCallCheck(this, DateRangePicker);

      var inputs = Array.isArray(options.inputs) ? options.inputs : Array.from(element.querySelectorAll('input'));

      if (inputs.length < 2) {
        return;
      }

      element.rangepicker = this;
      this.element = element;
      this.inputs = inputs.slice(0, 2);
      this.allowOneSidedRange = !!options.allowOneSidedRange;
      var changeDateListener = onChangeDate.bind(null, this);
      var cleanOptions = filterOptions(options); // in order for initial date setup to work right when pcicLvel > 0,
      // let Datepicker constructor add the instance to the rangepicker

      var datepickers = [];
      Object.defineProperty(this, 'datepickers', {
        get: function get() {
          return datepickers;
        }
      });
      setupDatepicker(this, changeDateListener, this.inputs[0], cleanOptions);
      setupDatepicker(this, changeDateListener, this.inputs[1], cleanOptions);
      Object.freeze(datepickers); // normalize the range if inital dates are given

      if (datepickers[0].dates.length > 0) {
        onChangeDate(this, {
          target: this.inputs[0]
        });
      } else if (datepickers[1].dates.length > 0) {
        onChangeDate(this, {
          target: this.inputs[1]
        });
      }
    }
    /**
     * @type {Array} - selected date of the linked date pickers
     */


    _createClass(DateRangePicker, [{
      key: "dates",
      get: function get() {
        return this.datepickers.length === 2 ? [this.datepickers[0].dates[0], this.datepickers[1].dates[0]] : undefined;
      }
      /**
       * Set new values to the config options
       * @param {Object} options - config options to update
       */

    }, {
      key: "setOptions",
      value: function setOptions(options) {
        this.allowOneSidedRange = !!options.allowOneSidedRange;
        var cleanOptions = filterOptions(options);
        this.datepickers[0].setOptions(cleanOptions);
        this.datepickers[1].setOptions(cleanOptions);
      }
      /**
       * Destroy the DateRangePicker instance
       * @return {DateRangePicker} - the instance destroyed
       */

    }, {
      key: "destroy",
      value: function destroy() {
        this.datepickers[0].destroy();
        this.datepickers[1].destroy();
        unregisterListeners(this);
        delete this.element.rangepicker;
      }
      /**
       * Get the start and end dates of the date range
       *
       * The method returns Date objects by default. If format string is passed,
       * it returns date strings formatted in given format.
       * The result array always contains 2 items (start date/end date) and
       * undefined is used for unselected side. (e.g. If none is selected,
       * the result will be [undefined, undefined]. If only the end date is set
       * when allowOneSidedRange config option is true, [undefined, endDate] will
       * be returned.)
       *
       * @param  {String} [format] - Format string to stringify the dates
       * @return {Array} - Start and end dates
       */

    }, {
      key: "getDates",
      value: function getDates() {
        var _this10 = this;

        var format = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
        var callback = format ? function (date) {
          return _formatDate(date, format, _this10.datepickers[0].config.locale);
        } : function (date) {
          return new Date(date);
        };
        return this.dates.map(function (date) {
          return date === undefined ? date : callback(date);
        });
      }
      /**
       * Set the start and end dates of the date range
       *
       * The method calls datepicker.setDate() internally using each of the
       * arguments in start→end order.
       *
       * When a clear: true option object is passed instead of a date, the method
       * clears the date.
       *
       * If an invalid date, the same date as the current one or an option object
       * without clear: true is passed, the method considers that argument as an
       * "ineffective" argument because calling datepicker.setDate() with those
       * values makes no changes to the date selection.
       *
       * When the allowOneSidedRange config option is false, passing {clear: true}
       * to clear the range works only when it is done to the last effective
       * argument (in other words, passed to rangeEnd or to rangeStart along with
       * ineffective rangeEnd). This is because when the date range is changed,
       * it gets normalized based on the last change at the end of the changing
       * process.
       *
       * @param {Date|Number|String|Object} rangeStart - Start date of the range
       * or {clear: true} to clear the date
       * @param {Date|Number|String|Object} rangeEnd - End date of the range
       * or {clear: true} to clear the date
       */

    }, {
      key: "setDates",
      value: function setDates(rangeStart, rangeEnd) {
        var _this$datepickers = _slicedToArray(this.datepickers, 2),
            datepicker0 = _this$datepickers[0],
            datepicker1 = _this$datepickers[1];

        var origDates = this.dates; // If range normalization runs on every change, we can't set a new range
        // that starts after the end of the current range correctly because the
        // normalization process swaps start↔︎end right after setting the new start
        // date. To prevent this, the normalization process needs to run once after
        // both of the new dates are set.

        this._updating = true;
        datepicker0.setDate(rangeStart);
        datepicker1.setDate(rangeEnd);
        delete this._updating;

        if (datepicker1.dates[0] !== origDates[1]) {
          onChangeDate(this, {
            target: this.inputs[1]
          });
        } else if (datepicker0.dates[0] !== origDates[0]) {
          onChangeDate(this, {
            target: this.inputs[0]
          });
        }
      }
    }]);

    return DateRangePicker;
  }();

  window.Datepicker = Datepicker;
  window.DateRangePicker = DateRangePicker;
})();

/***/ }),

/***/ "./resources/js/tailwind-datepicker/js/locales/ka.js":
/*!***********************************************************!*\
  !*** ./resources/js/tailwind-datepicker/js/locales/ka.js ***!
  \***********************************************************/
/***/ (() => {

/**
 * Georgian translation for bootstrap-datepicker
 * Levan Melikishvili <levani0101@yahoo.com>
 */
(function () {
  Datepicker.locales.ka = {
    days: ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"],
    daysShort: ["კვი", "ორშ", "სამ", "ოთხ", "ხუთ", "პარ", "შაბ"],
    daysMin: ["კვ", "ორ", "სა", "ოთ", "ხუ", "პა", "შა"],
    months: ["იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი"],
    monthsShort: ["იან", "თებ", "მარ", "აპრ", "მაი", "ივნ", "ივლ", "აგვ", "სექ", "ოქტ", "ნოე", "დეკ"],
    today: "დღეს",
    clear: "გასუფთავება",
    weekStart: 1,
    format: "dd.mm.yyyy"
  };
})();

/***/ }),

/***/ "./resources/css/app.css":
/*!*******************************!*\
  !*** ./resources/css/app.css ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/***/ ((module) => {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/axios/package.json":
/*!*****************************************!*\
  !*** ./node_modules/axios/package.json ***!
  \*****************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"_args":[["axios@0.21.4","C:\\\\Users\\\\nkochoradze\\\\Desktop\\\\Projects\\\\acss_napr"]],"_development":true,"_from":"axios@0.21.4","_id":"axios@0.21.4","_inBundle":false,"_integrity":"sha512-ut5vewkiu8jjGBdqpM44XxjuCjq9LAKeHVmoVfHVzy8eHgxxq8SbAVQNovDA8mVi05kP0Ea/n/UzcSHcTJQfNg==","_location":"/axios","_phantomChildren":{},"_requested":{"type":"version","registry":true,"raw":"axios@0.21.4","name":"axios","escapedName":"axios","rawSpec":"0.21.4","saveSpec":null,"fetchSpec":"0.21.4"},"_requiredBy":["#DEV:/"],"_resolved":"https://registry.npmjs.org/axios/-/axios-0.21.4.tgz","_spec":"0.21.4","_where":"C:\\\\Users\\\\nkochoradze\\\\Desktop\\\\Projects\\\\acss_napr","author":{"name":"Matt Zabriskie"},"browser":{"./lib/adapters/http.js":"./lib/adapters/xhr.js"},"bugs":{"url":"https://github.com/axios/axios/issues"},"bundlesize":[{"path":"./dist/axios.min.js","threshold":"5kB"}],"dependencies":{"follow-redirects":"^1.14.0"},"description":"Promise based HTTP client for the browser and node.js","devDependencies":{"coveralls":"^3.0.0","es6-promise":"^4.2.4","grunt":"^1.3.0","grunt-banner":"^0.6.0","grunt-cli":"^1.2.0","grunt-contrib-clean":"^1.1.0","grunt-contrib-watch":"^1.0.0","grunt-eslint":"^23.0.0","grunt-karma":"^4.0.0","grunt-mocha-test":"^0.13.3","grunt-ts":"^6.0.0-beta.19","grunt-webpack":"^4.0.2","istanbul-instrumenter-loader":"^1.0.0","jasmine-core":"^2.4.1","karma":"^6.3.2","karma-chrome-launcher":"^3.1.0","karma-firefox-launcher":"^2.1.0","karma-jasmine":"^1.1.1","karma-jasmine-ajax":"^0.1.13","karma-safari-launcher":"^1.0.0","karma-sauce-launcher":"^4.3.6","karma-sinon":"^1.0.5","karma-sourcemap-loader":"^0.3.8","karma-webpack":"^4.0.2","load-grunt-tasks":"^3.5.2","minimist":"^1.2.0","mocha":"^8.2.1","sinon":"^4.5.0","terser-webpack-plugin":"^4.2.3","typescript":"^4.0.5","url-search-params":"^0.10.0","webpack":"^4.44.2","webpack-dev-server":"^3.11.0"},"homepage":"https://axios-http.com","jsdelivr":"dist/axios.min.js","keywords":["xhr","http","ajax","promise","node"],"license":"MIT","main":"index.js","name":"axios","repository":{"type":"git","url":"git+https://github.com/axios/axios.git"},"scripts":{"build":"NODE_ENV=production grunt build","coveralls":"cat coverage/lcov.info | ./node_modules/coveralls/bin/coveralls.js","examples":"node ./examples/server.js","fix":"eslint --fix lib/**/*.js","postversion":"git push && git push --tags","preversion":"npm test","start":"node ./sandbox/server.js","test":"grunt test","version":"npm run build && grunt version && git add -A dist && git add CHANGELOG.md bower.json package.json"},"typings":"./index.d.ts","unpkg":"dist/axios.min.js","version":"0.21.4"}');

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/app": 0,
/******/ 			"css/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/js/app.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/app"], () => (__webpack_require__("./resources/css/app.css")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;