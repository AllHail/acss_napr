<!DOCTYPE html>
<html lang="en">
  <head>
    <title>ადმინისტრაციულ საჩივრებთან დაკავშირებით მიღებული გადაწყვეტილებების საძიებო სისტემა</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ადმინისტრაციულ საჩივრებთან დაკავშირებით მიღებული გადაწყვეტილებების საძიებო სისტემა">
    <meta name="keywords" content="NAPR">
    <meta name="author" content="IT">
    <meta name="email" content="info@napr.gov.ge">
    <meta name="website" content="https://napr.gov.ge">
    <meta name="version" content="v1.0">
    <link rel="icon" type="image/svg+xml" href="{{ asset('favicon/favicon.svg') }}">
    <link rel="icon" type="image/png" href="{{ asset('favicon/favicon.png') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body class="font-body text-gray-900 bg-gray-200 nice-scroll-thin">
    <div class="w-100 mx-auto p-2 md:max-w-6xl md:p-16 md:pt-8">
      <div class="rounded-t-md px-2 py-4 text-md font-mtavruli text-center bg-light-blue-500 text-white">ადმინისტრაციულ საჩივრებთან დაკავშირებით მიღებული გადაწყვეტილებების საძიებო სისტემა</div>
      <div class="shadow-md rounded-b-md p-2 bg-white md:p-16 md:pt-4">
        <form id="findclaim" method="post" autocomplete="off">
          <div class="flex md:flex-row flex-col">
            <div class="flex-1 mb-2 p-2">
              <label class="block text-gray-500 text-sm">საჩივრის/გადაწყვეტილების ნომერი</label>
              <input class="form-input w-full" name="app_no" type="text" placeholder="">
            </div>
            <div class="flex-1 mb-2 p-2">
              <label class="block text-gray-500 text-sm">საძიებო სიტყვა(ები)/ტექსტი</label>
              <input class="form-input w-full" name="keyword" type="text" placeholder="">
            </div>
          </div>
          <div class="flex md:flex-row flex-col">
            <div class="flex-1 mb-2 p-2">
              <label class="block text-gray-500 text-sm">დავის ტიპი/კატეგორია</label>
              <select class="form-select w-full" name="app_dispute_category" type="text">
                <option></option>
                <option value="1">ოფცია #1</option>
                <option value="2">ოფცია #2</option>
              </select>
            </div>
            <div class="flex-1 mb-2 p-2">
              <label class="block text-gray-500 text-sm">დავის საგანი</label>
              <select class="form-select w-full" name="app_dispute_subject" type="text">
                <option></option>
                <option value="1">ოფცია #1</option>
                <option value="2">ოფცია #2</option>
              </select>
            </div>
          </div>
          <div class="flex md:flex-row flex-col">
            <div class="flex-1 mb-2 p-2">
              <label class="block text-gray-500 text-sm">გადაწყვეტილების დასახელება/ტიპი</label>
              <select class="form-select w-full" name="app_dispute_type" type="text">
                <option></option>
                <option value="1">ოფცია #1</option>
                <option value="2">ოფცია #2</option>
              </select>
            </div>
            <div class="flex-1 mb-2 p-2">
              <label class="block text-gray-500 text-sm">პერიოდი</label>
              <div class="flex space-x-1 pb-1">
                <div class="flex-1">
                  <input class="start form-input w-full font-sans" id="period_start" name="app_date_from" type="text">
                </div>
                <div class="flex-1">
                  <input class="end form-input w-full font-sans" id="period_end" name="app_date_to" type="text">
                </div>
              </div>
            </div>
          </div>
        </form>
        <div class="flex mb-2 p-2 justify-center space-x-2">
          <button class="btn w-32" type="submit" id="submit_form">ძებნა</button>
          <button class="btn w-32" type="button" onClick="resetform()">თავიდან</button>
        </div>
        <div class="p-2 py-2 mb-2 border-b border-blue-gray-300 text-md">
          <div class="text-blue-gray-800">მოიძებნა 2 ჩანაწერი</div>
          <div class="text-red-400">ჩანაწერები არ მოიძებნა</div>
          <div class="text-red-400">მიუთითეთ საძებნი კრიტერიუმი</div>
        </div>
        <div class="divide-y divide-blue-gray-300">
          <div class="p-2">
            <div class="flex items-center">
              <div class="flex-1">
                <div class="my-4 line-clamp-2 text-xs text-gray-500">...ლორემ იპსუმ ფრანკოს სოფი მეხუთეთი,<span class="bg-yellow-200">ემიჯნებოდა</span>, ჰკვნესის მაგარსა გქონდა ვნებებზე გაქცევის მიგნებულია მარილინისთვის მითითებით უგუნურება დროინდელი. დროინდელი ვიღაცებმა თეთრში ჩამოახრჩვესო ველაფერი მიმაბამდა ვლადიმერ დაჰკითხეს მთარგმნელის, საბურთალოსკენ ფრანკოს, ნორმის ვნებებზე უყურებ. ტრიუკი დამაჭრეს მართლადაც ნორმის გაეხადა ეგებებოდნენ, ვლადიმერ, ლუარსაბია, მარილინისთვის მიგნებულია. პაშოლ მაგარსა სილიკონის რეპეტიციის უპრაგონო ბოროტმოქმედებად. მთხოვთ სოფი მდინარეზე, უნარია ფრანკოს, შეურაცხყოფა საუკეთესო, დავდგებოდი ოცნებაში. ლუარსაბია ველაფერი მითითებით ეკიდათ მეხუთეთი დროინდელი. დამწიფებული ჰკვნესის ვიეტნამია ვნებებზე, მაგარსა მთარგმნელის გიმღერო. მნათობის შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად ძმაკაცები, უგედ ავტომობილი ველაფერი სულდგმულობს სტალინურ უყურებ გაცნობოდნენ დალიოს.</div>
                <div class="mb-2 flex flex-col md:flex-row md:items-center">
                  <div class="w-48 text-light-blue-500 text-xs">განცხადების #</div>
                  <div class="text-md text-blue-gray-800 font-sans">0123456789</div>
                </div>
                <div class="mb-2 flex flex-col md:flex-row md:items-center">
                  <div class="w-48 text-light-blue-500 text-xs">თარიღი</div>
                  <div class="text-md text-blue-gray-800 font-sans font-light">18.09.21</div>
                </div>
              </div>
              <div class="flex flex-col items-center p-2 rounded group cursor-pointer hover:bg-light-blue-500"><a class="p-2" href="#" target="blank">
                  <svg class="h-10 w-10 text-light-blue-500 group-hover:text-white" width="24" height="24" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M14 3v4a1 1 0 0 0 1 1h4"></path>
                    <path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z"></path>
                    <line x1="12" y1="11" x2="12" y2="17"></line>
                    <polyline points="9 14 12 17 15 14"></polyline>
                  </svg></a>
                <div class="text-sm text-light-blue-600 font-sans font-light group-hover:text-white">21.09.21</div>
              </div>
            </div>
            <div class="mb-2 flex flex-col md:flex-row">
              <div class="text-light-blue-500 text-xs pr-2 md:w-48">გადაწყვეტილების დასახელება/ტიპი</div>
              <div class="flex-1 text-md text-blue-gray-800">შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად.</div>
            </div>
            <div class="mb-2 flex flex-col md:flex-row">
              <div class="text-light-blue-500 text-xs md:w-48">დავის ტიპი/კატეგორია</div>
              <div class="flex-1 text-md text-blue-gray-800 line-clamp-2">ლორემ იპსუმ ფრანკოს სოფი მეხუთეთი, ემიჯნებოდა, ჰკვნესის მაგარსა გქონდა ვნებებზე გაქცევის მიგნებულია მარილინისთვის მითითებით უგუნურება დროინდელი. დროინდელი ვიღაცებმა თეთრში ჩამოახრჩვესო ველაფერი მიმაბამდა ვლადიმერ დაჰკითხეს მთარგმნელის, საბურთალოსკენ ფრანკოს, ნორმის ვნებებზე უყურებ. ტრიუკი დამაჭრეს მართლადაც ნორმის გაეხადა ეგებებოდნენ, ვლადიმერ, ლუარსაბია, მარილინისთვის მიგნებულია. პაშოლ მაგარსა სილიკონის რეპეტიციის უპრაგონო ბოროტმოქმედებად. მთხოვთ სოფი მდინარეზე, უნარია ფრანკოს, შეურაცხყოფა საუკეთესო, დავდგებოდი ოცნებაში. ლუარსაბია ველაფერი მითითებით ეკიდათ მეხუთეთი დროინდელი. დამწიფებული ჰკვნესის ვიეტნამია ვნებებზე, მაგარსა მთარგმნელის გიმღერო. მნათობის შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად ძმაკაცები, უგედ ავტომობილი ველაფერი სულდგმულობს სტალინურ უყურებ გაცნობოდნენ დალიოს.</div>
            </div>
            <div class="mb-2 flex flex-col md:flex-row">
              <div class="text-light-blue-500 text-xs md:w-48">დავის საგანი</div>
              <div class="flex-1 text-sm text-blue-gray-800 line-clamp-2">ლორემ იპსუმ ფრანკოს სოფი მეხუთეთი, ემიჯნებოდა, ჰკვნესის მაგარსა გქონდა ვნებებზე გაქცევის მიგნებულია მარილინისთვის მითითებით უგუნურება დროინდელი. დროინდელი ვიღაცებმა თეთრში ჩამოახრჩვესო ველაფერი მიმაბამდა ვლადიმერ დაჰკითხეს მთარგმნელის, საბურთალოსკენ ფრანკოს, ნორმის ვნებებზე უყურებ. ტრიუკი დამაჭრეს მართლადაც ნორმის გაეხადა ეგებებოდნენ, ვლადიმერ, ლუარსაბია, მარილინისთვის მიგნებულია. პაშოლ მაგარსა სილიკონის რეპეტიციის უპრაგონო ბოროტმოქმედებად. მთხოვთ სოფი მდინარეზე, უნარია ფრანკოს, შეურაცხყოფა საუკეთესო, დავდგებოდი ოცნებაში. ლუარსაბია ველაფერი მითითებით ეკიდათ მეხუთეთი დროინდელი. დამწიფებული ჰკვნესის ვიეტნამია ვნებებზე, მაგარსა მთარგმნელის გიმღერო. მნათობის შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად ძმაკაცები, უგედ ავტომობილი ველაფერი სულდგმულობს სტალინურ უყურებ გაცნობოდნენ დალიოს.</div>
            </div>
          </div>
          <div class="p-2">
            <div class="flex items-center">
              <div class="flex-1">
                <div class="my-4 line-clamp-2 text-xs text-gray-500">...ლორემ იპსუმ ფრანკოს სოფი მეხუთეთი,<span class="bg-yellow-200">ემიჯნებოდა</span>, ჰკვნესის მაგარსა გქონდა ვნებებზე გაქცევის მიგნებულია მარილინისთვის მითითებით უგუნურება დროინდელი. დროინდელი ვიღაცებმა თეთრში ჩამოახრჩვესო ველაფერი მიმაბამდა ვლადიმერ დაჰკითხეს მთარგმნელის, საბურთალოსკენ ფრანკოს, ნორმის ვნებებზე უყურებ. ტრიუკი დამაჭრეს მართლადაც ნორმის გაეხადა ეგებებოდნენ, ვლადიმერ, ლუარსაბია, მარილინისთვის მიგნებულია. პაშოლ მაგარსა სილიკონის რეპეტიციის უპრაგონო ბოროტმოქმედებად. მთხოვთ სოფი მდინარეზე, უნარია ფრანკოს, შეურაცხყოფა საუკეთესო, დავდგებოდი ოცნებაში. ლუარსაბია ველაფერი მითითებით ეკიდათ მეხუთეთი დროინდელი. დამწიფებული ჰკვნესის ვიეტნამია ვნებებზე, მაგარსა მთარგმნელის გიმღერო. მნათობის შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად ძმაკაცები, უგედ ავტომობილი ველაფერი სულდგმულობს სტალინურ უყურებ გაცნობოდნენ დალიოს.</div>
                <div class="mb-2 flex flex-col md:flex-row md:items-center">
                  <div class="w-48 text-light-blue-500 text-xs">განცხადების #</div>
                  <div class="text-md text-blue-gray-800 font-sans">0123456789</div>
                </div>
                <div class="mb-2 flex flex-col md:flex-row md:items-center">
                  <div class="w-48 text-light-blue-500 text-xs">თარიღი</div>
                  <div class="text-md text-blue-gray-800 font-sans font-light">18.09.21</div>
                </div>
              </div>
              <div class="flex flex-col items-center p-2 rounded group cursor-pointer hover:bg-light-blue-500"><a class="p-2" href="#" target="blank">
                  <svg class="h-10 w-10 text-light-blue-500 group-hover:text-white" width="24" height="24" viewBox="0 0 24 24" stroke-width="1" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M14 3v4a1 1 0 0 0 1 1h4"></path>
                    <path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z"></path>
                    <line x1="12" y1="11" x2="12" y2="17"></line>
                    <polyline points="9 14 12 17 15 14"></polyline>
                  </svg></a>
                <div class="text-sm text-light-blue-600 font-sans font-light group-hover:text-white">21.09.21</div>
              </div>
            </div>
            <div class="mb-2 flex flex-col md:flex-row">
              <div class="text-light-blue-500 text-xs pr-4 md:w-48">გადაწყვეტილების დასახელება/ტიპი</div>
              <div class="flex-1 text-md text-blue-gray-800">შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად.</div>
            </div>
            <div class="mb-2 flex flex-col md:flex-row">
              <div class="text-light-blue-500 text-xs md:w-48">დავის ტიპი/კატეგორია</div>
              <div class="flex-1 text-md text-blue-gray-800 line-clamp-2">ლორემ იპსუმ ფრანკოს სოფი მეხუთეთი, ემიჯნებოდა, ჰკვნესის მაგარსა გქონდა ვნებებზე გაქცევის მიგნებულია მარილინისთვის მითითებით უგუნურება დროინდელი. დროინდელი ვიღაცებმა თეთრში ჩამოახრჩვესო ველაფერი მიმაბამდა ვლადიმერ დაჰკითხეს მთარგმნელის, საბურთალოსკენ ფრანკოს, ნორმის ვნებებზე უყურებ. ტრიუკი დამაჭრეს მართლადაც ნორმის გაეხადა ეგებებოდნენ, ვლადიმერ, ლუარსაბია, მარილინისთვის მიგნებულია. პაშოლ მაგარსა სილიკონის რეპეტიციის უპრაგონო ბოროტმოქმედებად. მთხოვთ სოფი მდინარეზე, უნარია ფრანკოს, შეურაცხყოფა საუკეთესო, დავდგებოდი ოცნებაში. ლუარსაბია ველაფერი მითითებით ეკიდათ მეხუთეთი დროინდელი. დამწიფებული ჰკვნესის ვიეტნამია ვნებებზე, მაგარსა მთარგმნელის გიმღერო. მნათობის შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად ძმაკაცები, უგედ ავტომობილი ველაფერი სულდგმულობს სტალინურ უყურებ გაცნობოდნენ დალიოს.</div>
            </div>
            <div class="mb-2 flex flex-col md:flex-row">
              <div class="text-light-blue-500 text-xs md:w-48">დავის საგანი</div>
              <div class="flex-1 text-sm text-blue-gray-800 line-clamp-2">ლორემ იპსუმ ფრანკოს სოფი მეხუთეთი, ემიჯნებოდა, ჰკვნესის მაგარსა გქონდა ვნებებზე გაქცევის მიგნებულია მარილინისთვის მითითებით უგუნურება დროინდელი. დროინდელი ვიღაცებმა თეთრში ჩამოახრჩვესო ველაფერი მიმაბამდა ვლადიმერ დაჰკითხეს მთარგმნელის, საბურთალოსკენ ფრანკოს, ნორმის ვნებებზე უყურებ. ტრიუკი დამაჭრეს მართლადაც ნორმის გაეხადა ეგებებოდნენ, ვლადიმერ, ლუარსაბია, მარილინისთვის მიგნებულია. პაშოლ მაგარსა სილიკონის რეპეტიციის უპრაგონო ბოროტმოქმედებად. მთხოვთ სოფი მდინარეზე, უნარია ფრანკოს, შეურაცხყოფა საუკეთესო, დავდგებოდი ოცნებაში. ლუარსაბია ველაფერი მითითებით ეკიდათ მეხუთეთი დროინდელი. დამწიფებული ჰკვნესის ვიეტნამია ვნებებზე, მაგარსა მთარგმნელის გიმღერო. მნათობის შუაღამემდე ჩაქუჩი მართლადაც მთხოვთ დამწიფებული. დამაჭრეს ბოროტმოქმედებად ძმაკაცები, უგედ ავტომობილი ველაფერი სულდგმულობს სტალინურ უყურებ გაცნობოდნენ დალიოს.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="js/app.js"></script>
  </body>
</html>