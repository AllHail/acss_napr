const { default: axios } = require('axios');

require('./tailwind-datepicker/js/datepicker-full');
require('./tailwind-datepicker/js/locales/ka');
require('axios');

const datepicker_options = {
    format: "dd/mm/yyyy",
    language: "ka",
    autohide: true,
    prevArrow: '<svg class="h-8 w-8 text-blue-500" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke="none" d="M0 0h24v24H0z"/>  <polyline points="15 6 9 12 15 18" /></svg >',
    nextArrow: '<svg class="h-8 w-8 text-blue-500" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke="none" d="M0 0h24v24H0z"/>  <polyline points="9 6 15 12 9 18" /></svg >',
    maxDate: new Date()
}

const pariod_start = document.getElementById('period_start');
new Datepicker(pariod_start, datepicker_options);

const pariod_end = document.getElementById('period_end');
new Datepicker(pariod_end, datepicker_options);

window.resetform = () => {
    document.getElementById('findclaim').reset()
}

const submit_button = document.getElementById('submit_form');

submit_button.addEventListener('click', function(event) {
    const form = document.getElementById('findclaim');
    let formData = new FormData(form);
    axios.post('/api/search', formData)
        .then(function(response) {
            let data = response.data;
            console.log(data);
        });
});
